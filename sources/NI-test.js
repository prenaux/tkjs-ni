/* global FIXTURE, TEST, TEST_ASYNC */
var assert = require("assert");
var NI = require('./NI');

FIXTURE("NI.format", function() {
  TEST("should return formated strings for simple placeholders", function() {
    assert.equal("10", NI.sprintf("%b", 2))
    assert.equal("A", NI.sprintf("%c", 65))
    assert.equal("2", NI.sprintf("%d", 2))
    assert.equal("2", NI.sprintf("%i", 2))
    assert.equal("2", NI.sprintf("%d", "2"))
    assert.equal("2", NI.sprintf("%i", "2"))
    assert.equal("2e+0", NI.sprintf("%e", 2))
    assert.equal("2", NI.sprintf("%u", 2))
    assert.equal("4294967294", NI.sprintf("%u", -2))
    assert.equal("2.2", NI.sprintf("%f", 2.2))
    assert.equal("10", NI.sprintf("%o", 8))
    assert.equal("%s", NI.sprintf("%s", "%s"))
    assert.equal("ff", NI.sprintf("%x", 255))
    assert.equal("FF", NI.sprintf("%X", 255))
    assert.equal("Polly wants a cracker", NI.sprintf("%2$s %3$s a %1$s", "cracker", "Polly", "wants"))
    assert.equal("Hello world!", NI.sprintf("Hello %(who)s!", {"who": "world"}))
  })

  TEST("should return formated strings for complex placeholders", function() {
    // sign
    assert.equal("2", NI.sprintf("%d", 2))
    assert.equal("-2", NI.sprintf("%d", -2))
    assert.equal("+2", NI.sprintf("%+d", 2))
    assert.equal("-2", NI.sprintf("%+d", -2))
    assert.equal("2", NI.sprintf("%i", 2))
    assert.equal("-2", NI.sprintf("%i", -2))
    assert.equal("+2", NI.sprintf("%+i", 2))
    assert.equal("-2", NI.sprintf("%+i", -2))
    assert.equal("2.2", NI.sprintf("%f", 2.2))
    assert.equal("-2.2", NI.sprintf("%f", -2.2))
    assert.equal("+2.2", NI.sprintf("%+f", 2.2))
    assert.equal("-2.2", NI.sprintf("%+f", -2.2))
    assert.equal("-2.3", NI.sprintf("%+.1f", -2.34))
    assert.equal("-0.0", NI.sprintf("%+.1f", -0.01))
    assert.equal("-000000123", NI.sprintf("%+010d", -123))
    assert.equal("______-123", NI.sprintf("%+'_10d", -123))
    assert.equal("-234.34 123.2", NI.sprintf("%f %f", -234.34, 123.2))

    // padding
    assert.equal("-0002", NI.sprintf("%05d", -2))
    assert.equal("-0002", NI.sprintf("%05i", -2))
    assert.equal("    <", NI.sprintf("%5s", "<"))
    assert.equal("0000<", NI.sprintf("%05s", "<"))
    assert.equal("____<", NI.sprintf("%'_5s", "<"))
    assert.equal(">    ", NI.sprintf("%-5s", ">"))
    assert.equal(">0000", NI.sprintf("%0-5s", ">"))
    assert.equal(">____", NI.sprintf("%'_-5s", ">"))
    assert.equal("xxxxxx", NI.sprintf("%5s", "xxxxxx"))
    assert.equal("1234", NI.sprintf("%02u", 1234))
    assert.equal(" -10.235", NI.sprintf("%8.3f", -10.23456))
    assert.equal("-12.34 xxx", NI.sprintf("%f %s", -12.34, "xxx"))

    // precision
    assert.equal("2.3", NI.sprintf("%.1f", 2.345))
    assert.equal("xxxxx", NI.sprintf("%5.5s", "xxxxxx"))
    assert.equal("    x", NI.sprintf("%5.1s", "xxxxxx"))

  })

  TEST("should return formated strings for callbacks", function() {
    assert.equal('function () { return "foobar" }', NI.sprintf("%s", function() { return "foobar" })) // eslint-disable-line
    assert.equal('function now() { [native code] }', NI.sprintf("%s", Date.now)) // should pass...
  })

  TEST("should return formated json strings for objects", function() {
    assert.equal('{"foo":"bar"}', NI.sprintf("%j", { foo: "bar" }));
    assert.equal('{\n "foo": "bar"\n}', NI.sprintf("%J", { foo: "bar" }));
  })

  TEST("should return formated json strings for arrays", function() {
    assert.equal('[1,2,3]', NI.sprintf("%j", [1,2,3]));
    assert.equal('[\n 1,\n 2,\n 3\n]', NI.sprintf("%J", [1,2,3]));
  })

  TEST("should return the msg if only one argument is passsed - and not do any formatting.", function() {
    assert.equal(undefined, NI.sprintf())
    assert.equal("%", NI.sprintf("%"))
    assert.equal("%b", NI.sprintf("%b"))
    assert.equal("%2B64447333", NI.sprintf("%2B64447333"))
  });

  TEST("should return formatted strings even for objects with cycle", function() {
    var obj = {
      foo: "foo",
    };
    obj.cycle = obj;

    var a = NI.format("%j", obj);
    NI.print("... a: " + a);
    assert.equal('{"foo":"foo","cycle":{"$ref":"$"}}', a);
    var b = NI.format("%k", obj);
    NI.print("... b: " + b);
    assert.equal('{/*2*/ foo: "foo", cycle: {/*1*/ $ref: "$" } }', b);
    var c = NI.format("%J", obj);
    NI.print("... c: " + c);
    assert.equal('{"foo":"foo","cycle":{"$ref":"$"}}', c.replace(/[\n]/g, '').replace(/\s/g, ''));
    var d = NI.format("%K", obj);
    NI.print("... d: " + d);
    assert.equal('{/*2*/foo:"foo",cycle:{/*1*/$ref:"$"}}', d.replace(/[\n]/g, '').replace(/\s/g, ''));
  })
})

FIXTURE("NI.b64", function() {
  TEST("b64encode/DecodeHex", function() {
    var hex = "a647755229fc64c67f6012493ad12fdcfe364659";
    var encodedHex = NI.b64encodeHex(hex);
    NI.println("... b64encodedHex: " + encodedHex);
    var decodedHex = NI.b64decodeHex(encodedHex);
    NI.println("... b64decodedHex: " + decodedHex);
    NI.assert.equals(hex.toLowerCase(), decodedHex);
  })
})

FIXTURE("NI.arraySliceBy", function() {
  TEST("0 elements", function() {
    assert.deepEqual(
      [],
      NI.arraySliceBy([], 3)
    )
  })

  TEST("aSize-1 elements", function() {
    assert.deepEqual(
      [[1,2]],
      NI.arraySliceBy([1,2], 3)
    )
  })

  TEST("aSize elements", function() {
    assert.deepEqual(
      [[1,2,3]],
      NI.arraySliceBy([1,2,3], 3)
    )
  })

  TEST("aSize+1 elements", function() {
    assert.deepEqual(
      [[1,2,3], [4]],
      NI.arraySliceBy([1,2,3,4], 3)
    )
  })

  TEST("when provided a func", function() {
    var slices = [];
    var result = NI.arraySliceBy([1,2,3,4], 3, function(slice) {
      slices.push(slice);
    });

    assert.deepEqual([[1,2,3], [4]], slices);
    assert.equal(undefined, result);
  })
})

FIXTURE("NI.Promise", function() {
  TEST_ASYNC("promise.all", function(aDone) {

    var p1 = Promise.resolve(3);
    var p2 = 1337;
    var p3 = NI.Promise(function(resolve /*, reject*/) {
      setTimeout(resolve, 100, "foo");
    });

    Promise.all([p1, p2, p3]).then(function(values) {
      console.log(values); // [3, 1337, "foo"]
      NI.assert.equals(3, values[0]);
      NI.assert.equals(1337, values[1]);
      NI.assert.equals("foo", values[2]);
      aDone();
    }).catch(function(err) {
      aDone(err);
    });
  })
})

FIXTURE("NI.isObjectLiteral", function() {
  TEST("should only be true for objects created from *new Object()* or *{}*", function() {
    assert(!NI.isObjectLiteral([]), "[]");
    assert(!NI.isObjectLiteral(new Array()), "new Array()");
    assert(!NI.isObjectLiteral(""), "\"\"");
    assert(!NI.isObjectLiteral(new String()), "new String()");
    assert(!NI.isObjectLiteral(function() {}), "function() {}");

    assert(NI.isObjectLiteral({}), "{}");
    assert(NI.isObjectLiteral(new Object()), "new Object()");
  })
})

FIXTURE("formatGQL", function() {
  var formatGQL = NI.formatGQL;

  TEST("with invalid aOp", function() {
    assert.throws(function() { formatGQL('foo') }, Error);
    assert.throws(function() { formatGQL() }, Error);
  })

  TEST("aOp as *query*", function() {
    assert.equal(
      "query _ { foo { id name } }",
      formatGQL('query', 'foo', ['id','name'])
    )
  })

  TEST("aOp as *mutation*", function() {
    assert.equal(
      "mutation _ { foo { id name } }",
      formatGQL('mutation', 'foo', ['id','name'])
    )
  })

  TEST("with invalid aFuncName", function() {
    assert.throws(function() { formatGQL('query') }, Error);
    assert.throws(function() { formatGQL('query', {}) }, Error);
  });

  TEST("without provided aArgs", function() {
    assert.equal(
      "query _ { foo { id name } }",
      formatGQL('query', 'foo', ['id','name'])
    )
  })

  TEST("with provided empty aArgs", function() {
    assert.equal(
      'query _ { foo { id name } }',
      formatGQL('query', 'foo', {}, ['id', 'name'])
    )
  })

  TEST("with provided simple aArgs", function() {
    assert.equal(
      'query _ { foo(a:1) { id name } }',
      formatGQL('query', 'foo', {a: 1}, ['id', 'name'])
    )
  })

  TEST("with provided complex aArgs", function() {
    assert.equal(
      'query _ { foo(a:"bar",b:{c:1,d:{e:"foo"}}) { id name } }',
      formatGQL('query', 'foo', {a: "bar", b: {c: 1, d: {e: "foo"}}}, ['id', 'name'])
    )
  })

  TEST("with invalid aFields", function() {
    assert.throws(function() {
      formatGQL('query', 'hello')
    }, Error);

    assert.throws(function() {
      formatGQL('query', 'foo', [{a: 1}, ['id', 'name']])
    }, Error)

    assert.throws(function() {
      formatGQL('query', 'foo', ['id', ['first_name', 'last_name']])
    }, Error)

    assert.throws(function() {
      formatGQL('query', 'foo', ['id', {name: 'invalid'}])
    }, Error)
  })

  TEST("with nested aFields", function() {
    assert.equal(
      "query _ { foo { id name { first last } address { line1 line2 } } }",
      formatGQL('query', 'foo', ['id', {name: ['first', 'last'], address: ['line1', 'line2']}])
    )
  })

  TEST("with nested aFields with simple input arg", function() {
    assert.equal(
      "query _ { foo { id name(id:9) { first last } } }",
      formatGQL('query', 'foo', ['id', {name: [{id: 9}, ['first', 'last']]}])
    )
  })

  TEST("with nested aFields with complex input arg", function() {
    assert.equal(
      'query _ { foo { id name(traits:{gender:"male",age:9}) { first last } } }',
      formatGQL('query', 'foo', ['id', {name: [{traits: {gender: 'male', age: 9}}, ['first', 'last']]}])
    )
  })

  TEST("with bulk queries", function() {
    assert.equal(
      "query _ { q0: user(name:\"foo\") { id }, q1: user(id:9) { name } }",
      formatGQL('query', [
        ['user', {name: "foo"}, ['id']],
        ['user', {id: 9}, ['name']]
      ])
    )
  })

})

FIXTURE("hasProperty", function() {
  TEST("with string", function() {
    assert(!NI.hasProperty('hello', 'trim'));
  })

  TEST("with number", function() {
    assert(!NI.hasProperty(1.1, 'toString'));
  })

  TEST("with boolean", function() {
    assert(!NI.hasProperty(true, 'toString'));
    assert(!NI.hasProperty(false, 'toString'));
  })

  TEST("with array", function() {
    assert(!NI.hasProperty([1], 'sort'));
  })

  TEST("with undefined", function() {
    assert(!NI.hasProperty(undefined, 'ignored')); // doesnt throw
  })

  TEST("with null", function() {
    assert(!NI.hasProperty(null, 'ignored')); // doesnt throw
  })

  TEST("with object created from Object.create(null)", function() {
    var obj = Object.create(null);
    obj.aa = 1;
    assert(NI.hasProperty(obj, 'aa'));
    assert(!NI.hasProperty(obj, 'bb'));
  })

  TEST("with object literal", function() {
    var obj = {aa: 1};
    assert(NI.hasProperty(obj, 'aa'));
    assert(!NI.hasProperty(obj, 'bb'));
  })

  function checkObject(obj) {
    var found = {};
    NI.forEach(obj,function(v,k) {
      found[k] = v;
    });

    assert(!NI.isEmpty(obj));
    assert(!NI.isEmpty(found));

    assert(NI.hasProperty(found, 'aa'));
    assert(NI.hasProperty(found, 'cc'));
    assert(!NI.hasProperty(found, 'bb'));

    var vals = NI.getObjectValues(obj);
    assert.equal(vals[0], 1);
    assert.equal(vals[1], 2);
    var keys = NI.getObjectKeys(obj);
    assert.equal(keys[0], 'aa');
    assert.equal(keys[1], 'cc');
    assert.equal(NI.size(obj), 2);
    assert.equal(NI.first(obj), 1);
    assert.equal(NI.last(obj), 2);
    assert.equal(NI.objectToQueryString(obj), 'aa=1&cc=2');

    assert.equal(2,NI.size(NI.jsonDecycle(obj)));
  }

  TEST("forEach with Object.create(null)", function() {
    var obj = Object.create(null);
    obj.aa = 1;
    obj.cc = 2;
    checkObject(obj);
  })

  TEST("forEach with {}", function() {
    var obj = {aa: 1, cc: 2};
    checkObject(obj);
  })

  TEST("clone should handle objects with no prototype", function() {
    var obj = Object.create(null);
    obj.aa = 1;
    obj.bb = 2;
    var clone = NI.clone(obj);
    assert.equal(obj.aa, 1);
    assert.equal(obj.bb, 2);
  })

  TEST("clone should handle nested objects with no prototype", function() {
    var obj = Object.create(null);
    obj.aa = 1;
    obj.bb = { cc: 1, dd: 2 };
    obj.ee = Object.create(null);
    obj.ee.ff = 3;

    var clone = NI.clone(obj);
    assert.equal(clone.aa, 1);
    assert.equal(clone.bb.cc, 1);
    assert.equal(clone.bb.dd, 2);
    assert.equal(clone.ee.ff, 3);
  })

})
