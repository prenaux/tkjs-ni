/*
  Code based on: https://github.com/treygriffith/filequeue/

  (The MIT License)

  Copyright (c) 2012 Trey Griffith; Wikify, Inc.

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/*

Filequeue
==============
### Drop-in Replacement for `fs` that avoids `Error: EMFILE, too many open files`.

`Filequeue` was born out of my encounter with `Error: EMFILE, too many open files`, which occurs when you try to open too many files at once on your system. Due to Node's asynchronous nature, if you perform a lot of `fs.readFile` or similar operations in quick succession, you can easily hit your system's `maxfiles` limit, usually set to 256 on a dev box.

`Filequeue` creates a replacement for `fs`, that I use as `fq` with many of the same operations. However, it keeps track of how many files are open at once, and queues them if there are too many.

How to Use
-----------

#### Instantiate Filequeue with a maximum number of files to be opened at once (default is 200)

``` javascript
  var fq = require('tkjs-ni/sources/fq').init(100);

  // additional instances will attempt to use the same instance (and therefore the same maxfiles)
  var fq2 = require('tkjs-ni/sources/fq').init(200);
  console.log(fq === fq2); // => true

  // you can force a new instance of filequeue with the `newQueue` parameter
  var fq3 =   var fq = require('tkjs-ni/sources/fq').init(100,true);
  console.log(fq === fq3); // => false

```

#### Use any of the following supported `fs` methods
* [readFile](http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_readfile_filename_options_callback)
* [writeFile](http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_writefile_filename_data_options_callback)
* [readdir](http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_readdir_path_callback)
* [rename](http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_rename_oldpath_newpath_callback)
* [symlink](http://nodejs.org/docs/latest/api/fs.html#fs_fs_symlink_srcpath_dstpath_type_callback)
* [mkdir](http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_mkdir_path_mode_callback)
* [stat](http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_stat_path_callback)
* [exists](http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_exists_path_callback)
* [createReadStream](http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_createreadstream_path_options)
* [createWriteStream](http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_createwritestream_path_options)

``` javascript
  for(var i=0; i<1000; i++) {
    fq.readFile('/somefile.txt', {encoding: 'utf8'}, function(err, somefile) {
      console.log("data from somefile.txt without crashing!", somefile);
    });
  }
```

Other Methods
-------------
Adding a new `fs` method is simple, just add it to the `methods.js` file following the conventions therein.

Pull requests to add other fs methods with tests exercising them are welcome.

*/

var FS = require('./fs');
var UTIL = require('util');

// All of the fs methods we want exposed on fq
// This should pass the Grep Test (http://jamie-wong.com/2013/07/12/grep-test/)
var STREAM = (function() {

  var fq = {};

  UTIL.inherits(FQReadStream, FS.ReadStream);
  fq.ReadStream = FQReadStream;

  function FQReadStream(path, options) {

    // temporarily change our #open method so the ReadStream initializer doesn't open the file
    var open = this.open;
    this.open = function() {};

    FS.ReadStream.call(this, path, options);

    this.open = open;

    return this;
  }

  UTIL.inherits(FQWriteStream, FS.WriteStream);
  fq.WriteStream = FQWriteStream;

  function FQWriteStream(path, options) {

    // temporarily change our #open method so the ReadStream initializer doesn't open the file
    var open = this.open;
    this.open = function() {};

    FS.WriteStream.call(this, path, options);

    this.open = open;

    return this;
  }

  return fq;
})

var addFsMethods = function (FQ) {

  /**
   * Asynchronously reads the entire contents of a file
   * http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_readfile_filename_options_callback
   */
  FQ.prototype.readFile = function(filename, options, callback) {

    if(!callback) {
      callback = options;
      options = null;
    }

    this.addToQueue(function(fs, cb) {

      if(options) {
        fs.readFile(filename, options, cb);
        return;
      }

      fs.readFile(filename, cb);

    }, callback);

    this.execQueue();
  };

  /**
   * Asynchronous rename
   * http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_rename_oldpath_newpath_callback
   */
  FQ.prototype.rename = function(oldPath, newPath, callback) {

    this.addToQueue(function(fs, cb) {

      fs.rename(oldPath, newPath, cb);

    }, callback);

    this.execQueue();
  };

  /**
   * Asynchronous symlink
   * http://nodejs.org/docs/latest/api/fs.html#fs_fs_symlink_srcpath_dstpath_type_callback
   */
  FQ.prototype.symlink = function(srcpath, dstpath, type, callback) {

    if(!callback) {
      callback = type;
      type = null;
    }

    this.addToQueue(function(fs, cb) {

      if(type) {
        fs.symlink(srcpath, dstpath, type, cb);
        return;
      }

      fs.symlink(srcpath, dstpath, cb);

    }, callback);

    this.execQueue();
  };

  /**
   * Asynchronously write data to file
   * http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_writefile_filename_data_options_callback
   */
  FQ.prototype.writeFile = function(filename, data, options, callback) {

    if(!callback) {
      callback = options;
      options = null;
    }

    this.addToQueue(function(fs, cb) {

      if(options) {
        fs.writeFile(filename, data, options, cb);
        return;
      }

      fs.writeFile(filename, data, cb);

    }, callback);

    this.execQueue();
  };

  /**
   * Asynchronous stat
   * http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_stat_path_callback
   */
  FQ.prototype.stat = function(path, callback) {

    this.addToQueue(function(fs, cb) {

      fs.stat(path, cb);

    }, callback)

    this.execQueue();
  };

  /**
   * Asynchronous readdir
   * http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_readdir_path_callback
   */
  FQ.prototype.readdir = function(path, callback) {

    this.addToQueue(function(fs, cb) {

      fs.readdir(path, cb);

    }, callback);

    this.execQueue();
  };

  /**
   * Test whether or not the given path exists by checking with the file system.
   * http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_exists_path_callback
   */
  FQ.prototype.exists = function(path, callback) {

    this.addToQueue(function(fs, cb) {

      fs.exists(path, cb);

    }, callback);

    this.execQueue();
  };

  /**
   * Asynchronous mkdir
   * http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_mkdir_path_mode_callback
   */
  FQ.prototype.mkdir = function(path, mode, callback) {

    if(!callback) {
      callback = mode;
      mode = null;
    }

    this.addToQueue(function(fs, cb) {

      if(mode) {
        fs.mkdir(path, mode, cb);
        return;
      }

      fs.mkdir(path, cb);

    }, callback);

    this.execQueue();
  };

  /**
   * http://nodejs.org/docs/v0.10.15/api/fs.html#fs_class_fs_readstream
   */
  FQ.prototype.ReadStream = STREAM.ReadStream;

  /**
   * http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_createreadstream_path_options
   */
  FQ.prototype.createReadStream = function(path, options) {

    var readStream = new this.ReadStream(path, options);

    this.addToQueue(function(fs, cb) {

      readStream.on('close', cb);

      readStream.open();
    });

    this.execQueue();

    return readStream;
  };

  /**
   * http://nodejs.org/docs/v0.10.15/api/fs.html#fs_class_fs_writestream
   */
  FQ.prototype.WriteStream = STREAM.WriteStream;

  /**
   * http://nodejs.org/docs/v0.10.15/api/fs.html#fs_fs_createwritestream_path_options
   */
  FQ.prototype.createWriteStream = function(path, options) {

    var writeStream = new this.WriteStream(path, options);

    this.addToQueue(function(fs, cb) {

      writeStream.on('close', cb);

      writeStream.open();
    });

    this.execQueue();

    return writeStream;
  };

};


var _fq;

// Instantiate a new FileQueue. Pass in the maximum number of files to be opened at any one time.
// By default it attempts to return an already instantiated instance of FileQueue so that your maxfiles is shared across processes
// However, by setting the `newQueue` to true you can request an entirely new queue (and one that won't be returned to any other instantiators)
function FileQueue(limit, newQueue) {
	if(typeof limit === 'boolean') {
		newQueue = limit;
		limit = null;
	}

	if(!limit) {
		limit = 200; // default file queue limit of 200, based on normal maxfiles of 256
	}

	// if there is already an instance of FileQueue running, update the limit and return it
	if (!newQueue && (_fq instanceof FileQueue)) {
		if (limit > _fq.limit) {
			_fq.limit = limit;
		}
		return _fq;
	}

	this.limit = limit;
	this.queue = [];
	this.openFiles = 0;

	// create a reference to the instance to be accessed again
	if (!newQueue) {
		_fq = this; // eslint-disable-line
	}

	return this;
}

FileQueue.prototype.addToQueue = function(fn, callback) {

	this.queue.push({
		fn: fn,
		callback: callback
	});

	return this;
};

FileQueue.prototype.execQueue = function() {

	var that = this;

	// only execute if the queue has any files
	if(!this.queue.length) {
		return;
	}

	// execute the first file in the queue
	var command = this.queue.shift();

	// check that we're not over our limit
	if(this.openFiles < this.limit) {

		// account for this file being open
		this.openFiles++;

		command.fn(FS, function() {

			that.openFiles--;
			that.execQueue();

			if(command.callback) {
				command.callback.apply(this, [].slice.call(arguments));
			}
		});
	} else {

		// we can't execute it yet, so we put it back in the front of the line
		this.queue.unshift(command);
	}
};

addFsMethods(FileQueue);

function init(aLimit,aNewQueue) {
  return new FileQueue(aLimit,aNewQueue);
}
exports.init = init;
