/* global __BACKEND__ */
var NI = require('./NI-core.js');

//======================================================================
// Json read/write to file
//======================================================================
if (__BACKEND__) {
  (function() {
    var FS = require('fs');

    function makeDirSync(dir) {
      if (!FS.existsSync(dir)) {
        try {
          FS.mkdirSync(dir);
        }
        catch (e) {
          NI.error("Can't create directory '%s'.", dir);
          return undefined;
        }
      }
      return true;
    };
    NI.makeDirSync = makeDirSync;

    function jsonReadFile(file, options, callback) {
      if (callback == undefined) {
        callback = options;
        options = null;
      }
      // a callback must be present...
      NI.assert.isFunction(callback);
      try {
        FS.readFile(file, options, function(err, data) {
          if (err) {
            return callback(err, null);
          }

          var obj = null;
          try {
            obj = JSON.parse(data);
          }
          catch (err2) {
            return callback(err2, null);
          }

          callback(null, obj);
        });
      }
      catch (e) {
        return undefined;
      }
      return true;
    };
    NI.jsonReadFile = jsonReadFile;

    function jsonReadFileSync(file, options, callback) {
      try {
        var readContent = FS.readFileSync(file, options);
        return JSON.parse(readContent);
      }
      catch (err) {
        if (callback) {
          callback(err);
        }
        else {
          NI.error("NI.jsonReadFileSync: Can't read file '" + file + "': " + err);
        }
        return undefined;
      }
    };
    NI.jsonReadFileSync = jsonReadFileSync;

    function jsonWriteFile(file, obj, options, callback) {
      if (callback == undefined) {
        callback = options;
        options = null;
      }
      if (!callback) {
        // if we don't put a callback the whole server crashes when there's an
        // error... even if it can't write the file just because it doesnt
        // have rights to or the folder doesnt exist - unreal...
        callback = function(err) {
          if (err) {
            NI.error("Can't write file '%s': %s", file, err);
          }
        };
      }

      var str = '';
      try {
        str = JSON.stringify(obj, null, NI.selectn('spaces',options)) + '\n';
        FS.writeFile(file, str, options, callback);
      }
      catch (err) {
        NI.error("Can't write file '%s': %s", file, err);
        return undefined;
      }

      return true;
    };
    NI.jsonWriteFile = jsonWriteFile;

    function jsonWriteFileSync(file, obj, options) {
      try {
        var str = JSON.stringify(obj, null, NI.selectn('spaces',options)) + '\n';
        return FS.writeFileSync(file, str, options);
      }
      catch (e) {
        NI.error("NI.jsonWriteFileSync: Can't write file '" + file + "': " + e);
        return undefined;
      }
      return true;
    };
    NI.jsonWriteFileSync = jsonWriteFileSync;
  })();
}

//======================================================================
// Default logGetCallee
//======================================================================
if (__BACKEND__) {
  var stackTrace = require('stack-trace');
  var PATH = require('path');
  NI.logGetCallee = function logGetCallee(aStackPos) {
    var frame = stackTrace.get()[(aStackPos !== undefined) ? aStackPos : 2];
    var file = PATH.basename(frame.getFileName())
    if (file === undefined || file == "undefined") {
      file = ""
    }
    var line = frame.getLineNumber()
    var method = frame.getFunctionName()
    if (method && method.length) {
      return NI.sprintf("[%s:%d in %s]", file, line, method)
    }
    else {
      return NI.sprintf("[%s:%d]", file, line)
    }
  }
};

//======================================================================
//  Misc utils
//======================================================================

/**
 * Returns N random bytes.
 */
NI.randomBytes = (function() {
  // We feature detect to determine the best RNG source.
  var _rng;

  // Node.js crypto-based RNG - http://nodejs.org/docs/v0.6.2/api/crypto.html
  //
  // Moderately fast, high quality
  if (typeof(require) == 'function') {
    try {
      var _rb = require('crypto').randomBytes;
      _rng = _rb && function(N) {
        return _rb(N);
      };
    }
    catch(e) {
      // Do Nothing
    }
  }

  if (!_rng) {
    // Math.random()-based (RNG)
    //
    // If all else fails, use Math.random().  It's fast, but is of unspecified
    // quality.
    _rng = function(N) {
      var r = 0
      var  _rnds = new Uint8Array(N);
      for (var i = 0; i < N; i++) {
        if ((i & 0x03) === 0) {
          r = Math.random() * 0x100000000;
        }
        _rnds[i] = r >>> ((i & 0x03) << 3) & 0xff;
      }
      return _rnds;
    };
  }

  return _rng;
})();

if (__BACKEND__) {
  NI.getEnvConfig = function getEnvConfig(aName,aDefault) {
    aName = "ENV_CONFIG_"+aName;
    var val = process.env[aName];
    if (NI.isEmpty(val)) {
      val = aDefault;
    }
    if (NI.isEmpty(val)) {
      throw NI.Error("Can't get environment variable configuration from '%s'.", aName);
    }
    return val;
  }
}

module.exports = NI;
