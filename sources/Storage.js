/* global __BACKEND__ */
var NI = require('./NI-core');

/*

setItem(key,value) -> Promise(value)
getItem(key) -> Promise(value)
removeItem(key) -> Promise(value)
clear -> Promise

*/

if (__BACKEND__) { (function() {
  var FS = require('./fs');
  var PATH = require('./path');
  var _storageDir = undefined;
  var FQ = require('./fq').init(64,true);

  function init(aOptions) {
    NI.assert.has(aOptions, "dir");
    _storageDir = PATH.normalize(aOptions.dir);
    FS.mkdirpSync(_storageDir);
    NI.log("Storage with file system at '%s'.", _storageDir);
    return this;
  }
  exports.init = init;

  function _buildKeyPath(key) {
    return PATH.join(_storageDir, NI.stringEncodeURI(key));
  }

  function setItem(key,value) {
    var filePath = _buildKeyPath(key);
    return NI.Promise(function(aResolve,aReject) {
      FQ.writeFile(filePath, NI.jsonToString(value), { encoding: 'utf8' }, function(err) {
        if (err) {
          aReject(err);
        }
        else {
          aResolve(value);
        }
      });
    });
  }
  exports.setItem = setItem;

  function getItem(key) {
    var filePath = _buildKeyPath(key);
    return NI.Promise(function(aResolve,aReject) {
      FQ.stat(filePath, function(errStat,stats) {
        if (errStat || !stats || !stats.isFile()) {
          aResolve(undefined);
        }
        else {
          FQ.readFile(filePath, { encoding: 'utf8' }, function(err,data) {
            if (err) {
              aReject(err);
            }
            else {
              aResolve(NI.jsonParse(data));
            }
          });
        }
      })
    });
  }
  exports.getItem = getItem;

  function removeItem(key) {
    var filePath = _buildKeyPath(key);
    return getItem(key).then(function(value) {
      return NI.Promise(function(aResolve,aReject) {
        FS.unlink(filePath, function(err) {
          if (err) {
            aReject(err);
          }
          else {
            aResolve(value);
          }
        });
      });
    });
  }
  exports.removeItem = removeItem;

  function clear() {
    /* Not Implemented */
  }
  exports.clear = clear;

})(); }
else { (function() {

  var _fallbackStorage;
  var _ls;

  // Firefox sets localStorage to 'null' if support is disabled IE might go
  // crazy if quota is exceeded and start treating it as 'unknown'
  if (window.localStorage === null || typeof window.localStorage === 'unknown') {
    NI.error('localStorage is disabled');
    _fallbackStorage = {};
  }
  else {
    _ls = window.localStorage;
  }

  function init() {
    return this;
  }
  exports.init = init;

  function setItem(key,value) {
    var r = value;
    if (_fallbackStorage) {
      _fallbackStorage[key] = value;
    }
    else {
      _ls.setItem(key,NI.jsonToString(value));
    }
    return NI.Promise(function(aResolve/*,aReject*/) {
      aResolve(r);
    });
  }
  exports.setItem = setItem;

  function getItem(key) {
    var r;
    if (_fallbackStorage) {
      r = _fallbackStorage[key];
    }
    else {
      r = NI.jsonParse(_ls.getItem(key));
    }
    return NI.Promise(function(aResolve/*,aReject*/) {
      aResolve(r);
    });
  }
  exports.getItem = getItem;

  function removeItem(key) {
    var r = _ls.getItem(key);
    if (_fallbackStorage) {
      delete _fallbackStorage[key];
    }
    else {
      _ls.removeItem(key);
    }
    return NI.Promise(function(aResolve/*,aReject*/) {
      aResolve(r);
    });
  }
  exports.removeItem = removeItem;

  function clear() {
    var r;
    if (_fallbackStorage) {
      _fallbackStorage = {};
    }
    else {
      _ls.clear();
    }
    return NI.Promise(function(aResolve/*,aReject*/) {
      aResolve(r);
    });
  }
  exports.clear = clear;

})(); }
