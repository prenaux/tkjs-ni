var NI = require('./NI');
var GLOB = require('glob');

function runAllTests(aConfig) {
  aConfig = aConfig || {};
  aConfig.sources = aConfig.sources || GLOB.sync('sources/**/*-test.@(js|jsx)');

  // Otherwise node crashes silently... wow yup...
  process.on('uncaughtException', function (exception) {
    console.log(exception);
    console.log(exception.stack);
  });

  var MochaClass = require('mocha');

  var MyReporter = (function() {
    var Base = require('mocha/lib/reporters/base');

    var title = function(test) {
      return test.fullTitle().replace(/#/g, '');
    }

    var reporter = function(runner) {
      Base.call(this, runner);

      var n = 0;
      var passes = 0;
      var failures = [];
      var times = {}
      var allStartTime = NI.timerInSeconds();

      runner.on('start', function() {
        var total = runner.grepTotal(runner.suite);
        NI.println('# %d tests', total);
      });

      runner.on('test', function(test) {
        ++n;
        var t = title(test);
        NI.println('# test %d %s', n, t);
        times[t] = NI.timerInSeconds();
      });

      // runner.on('test end', function(test) {
      // });

      runner.on('pending', function(test) {
        NI.println('ok %d %s # SKIP -', n, title(test));
      });

      runner.on('pass', function(test) {
        passes++;
        var t = title(test);
        var startTime = times[t];
        NI.println('# ok %d %s [%.5fs]', n, title(test), NI.timerInSeconds()-startTime);
      });

      runner.on('fail', function(test, err) {
        var name = title(test);
        failures.push(name);
        NI.println('# failed %d %s', n, name);
        if (err.stack) {
          NI.println(err.stack.replace(/^/gm, '  '));
        }
      });

      runner.on('end', function() {
        NI.println('# tests %d in %.3fs', (passes + failures.length), NI.timerInSeconds()-allStartTime);
        NI.println('# pass ' + passes);
        NI.println('# fail ' + failures.length + ": %j", failures);
        if (failures.length > 0) {
          NI.error("%j failure(s)", failures.length);
        }
        else {
          NI.info("All %d test(s) succeeded.", passes);
        }
      });
    }

    return reporter;
  }());

  NI.print(NI.stringPadBefore(NI.format(" UnitTests [%s]", NI.dateToString(new Date())), "#", 70));

  // Glob for the test files
  var sources = aConfig.sources;
  var skippedFiles = [];

  // Test API
  var filterSrcFile = undefined
  var filterSrcFileStr = aConfig.filterSrc
  if (filterSrcFileStr) {
    filterSrcFile = new RegExp(filterSrcFileStr, 'i');
    NI.log("Filter source: " + filterSrcFileStr);
  }

  var filterTest = undefined
  var filterTestStr = aConfig.filterTest || aConfig.filterTests;
  if (filterTestStr) {
    filterTest = new RegExp(filterTestStr, 'i');
    NI.log("Filter test: " + filterTestStr);
  }

  function FIXTURE(aFixtureName,aFunction) {
    global.describe(aFixtureName, aFunction)
  }
  global.FIXTURE = FIXTURE;

  function TEST(aName,aFunction) {
    global.it(aName, function(done) {
      aFunction();
      done();
    });
  }
  global.TEST = TEST;

  function TEST_ASYNC(aName,aFunction) {
    global.it(aName, function(done) {
      aFunction(done);
    });
  }
  global.TEST_ASYNC = TEST_ASYNC;

  function TEST_PROMISE(aName,aPromise) {
    global.it(aName, function(done) {
      return aPromise()
        .then(function(/*res*/) {
          done();
        })
        .catch(function(e) {
          done(e);
        });
    });
  }
  global.TEST_PROMISE = TEST_PROMISE;

  function STARTUP(aFunction) {
    global.before(aFunction)
  }
  global.STARTUP = STARTUP;

  function SHUTDOWN(aFunction) {
    global.after(aFunction);
  }
  global.SHUTDOWN = SHUTDOWN;

  // Kill running unittest server process:
  //   kill `ps | grep 'node \./.*unittest' | awk '{print $1;}'`
  var _serverCount = 0;
  function STARTUP_TESTSERVER(aArgs,aFunction) {
    global.before(function(done) {
      var spawn = require("child_process").spawn;
      this.timeout(5000);

      var serverCount = _serverCount++;
      var serverProcess = spawn(
        "node", ["./sources/server_unittest.js"].concat(aArgs || []),
        { async: true });

      var started = false;
      serverProcess.stdout.setEncoding('utf8');
      serverProcess.stdout.on('data', function (chunk) {
        process.stdout.write("S" + serverCount + "-OUT/" + chunk);
        if (!started && NI.stringStartsWith(chunk,"I/Server started")) {
          started = true;
          (aFunction && aFunction(serverProcess));
          done();
        }
      });
      serverProcess.stdout.on('end', function () {
      });

      serverProcess.stderr.setEncoding('utf8');
      serverProcess.stderr.on('data', function (chunk) {
        process.stdout.write("S" + serverCount + "-ERR/" + chunk);
      });
      serverProcess.stderr.on('end', function () {
      });
    });
  }
  global.STARTUP_TESTSERVER = STARTUP_TESTSERVER;

  // Run the tests
  (function() {
    var mocha = new MochaClass({
      reporter: MyReporter
    });

    if (filterTest) {
      mocha = mocha.grep(filterTest);
    }

    NI.forEach(sources, function(srcFileName) {
      if (filterSrcFile && !filterSrcFile.test(srcFileName)) {
        skippedFiles.push(srcFileName);
        return;
      }
      NI.print(NI.stringPadBefore(NI.format(" '%s'", srcFileName), "#", 70));
      mocha.addFile(srcFileName);
    });

    mocha.run(function(failures) {
      process.exit(failures); // eslint-disable-line
    });
  }());
}
exports.runAllTests = runAllTests;
