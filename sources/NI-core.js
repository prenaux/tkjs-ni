/* global __BACKEND__, __DEV__, Promise */
var NI = exports;

(function () {
  if (typeof window !== 'undefined') {
    window.NI = NI;
    NI.isNode = false;
    NI.global = window;
    if (typeof __DEV__ === 'undefined') {
      throw new Error("__DEV__ must be defined by the environments on the frontend.");
    }
    else {
      NI.global.__DEV__ = __DEV__;
    }
    if (typeof __BACKEND__ === 'undefined') {
      NI.global.__BACKEND__ = false;
    }
  }
  else {
    module.exports = NI;
    global.NI = NI;
    NI.isNode = true;
    NI.global = global;
    if (typeof NI.global.__DEV__ === 'undefined') {
      NI.global.__DEV__ = process.env.NODE_ENV !== 'production';
    }
    if (typeof __BACKEND__ === 'undefined') {
      NI.global.__BACKEND__ = true;
    }
  }

  NI.global.isProduction = !__DEV__;
  if (typeof require === "undefined") {
    require = function(aPath) {
      throw new Error("'require' not supported! trying to require '" + aPath + "'.");
    }
  }
})();

//======================================================================
// Log & Errors
//======================================================================
function consoleLog(aMsg) {
  console.log(aMsg);
}
exports.consoleLog = consoleLog;

function defaultLogEx(aMsg,aType) {
  switch (aType) {
  case "error": {
    consoleLog("E/" + aMsg);
    break;
  }
  case "warn": {
    consoleLog("W/" + aMsg);
    break;
  }
  case "debug": {
    consoleLog("D/" + aMsg);
    break;
  }
  default: {
    consoleLog("I/" + aMsg);
    break;
  }
  }
  return aMsg;
}
exports.defaultLogEx = defaultLogEx;

exports.initLog = function(aLogHook) {
  if (!aLogHook) {
    exports.logEx = exports.defaultLogEx;
  }
  else {
    exports.logEx = function(aMsg, aType) {
      var r = aLogHook(aMsg, aType);
      if (r === true) {
        // if log hook explicitly returns true we don't call defaultLogEx
        return aMsg;
      }
      return exports.defaultLogEx(aMsg,aType);
    }
    logInfo("Custom log hook initialized.");
  }
}

function _formatLogMsg(aMsg) {
  if (exports.logGetCallee) {
    var callee = exports.logGetCallee(3);
    aMsg += " " + callee;
  }
  return aMsg;
}

function ErrorFmt(aMsg) {
  aMsg = _formatLogMsg(NI.sprintf.apply(this,arguments));
  return new Error(aMsg);
}
exports.Error = ErrorFmt;

function print(aMsg) {
  aMsg = NI.sprintf.apply(this,arguments);
  consoleLog(aMsg);
  return aMsg;
};
exports.print = print;
exports.println = print;

NI.logGetCallee = undefined;

/**
 * Log the specified message
 *
 * @param {string} aMsg
 * @param {string=} aType
 */
exports.logEx = defaultLogEx;

function logInfo(aMsg) {
  aMsg = _formatLogMsg(NI.sprintf.apply(this,arguments));
  return NI.logEx(aMsg,"info");
};
exports.info = logInfo;
exports.log = logInfo;

function logError(aMsg) {
  aMsg = _formatLogMsg(NI.sprintf.apply(this,arguments));
  return NI.logEx(aMsg,"error");
};
exports.error = logError;

function logWarn(aMsg) {
  aMsg = _formatLogMsg(NI.sprintf.apply(this,arguments));
  return NI.logEx(aMsg,"warn");
};
exports.warn = logWarn;
exports.warning = logWarn;

function logDebug(aMsg) {
  aMsg = _formatLogMsg(NI.sprintf.apply(this,arguments));
  return NI.logEx(aMsg,"debug");
};
exports.dbg = logDebug;
exports.debug = logDebug;
exports.trace = logDebug;

function sendError(res, err, aMsg) {
  if (aMsg) {
    aMsg = aMsg + ": "
  }
  else {
    aMsg = ""
  }
  if (res && ("send" in res) && ("status" in res)) {
    res.status(400).send({ error: aMsg + err });
    res = NI.error;
  }

  var onError = res || NI.error;
  var msg = aMsg + err;
  if (err.stack) {
    msg += "\n--- stack ---\n" + err.stack;
  }
  try {
    onError(msg,err);
  }
  catch (e) {
    NI.error("onError: Exception: %s", e);
    if (e && e.stack) {
      consoleLog(e.stack);
    }
  }
  if (onError != NI.error) {
    NI.error(msg);
  }
}
exports.sendError = sendError;

function promiseCatch(e) {
  var errMsg = NI.format("PromiseError: %s", formatError(e));
  if (NI.global && NI.global.flashMessage) {
    NI.global.flashMessage("error", errMsg);
  }
  NI.error(errMsg);
}
exports.promiseCatch = promiseCatch;

//======================================================================
// Core
//======================================================================
/**
 * Return the time since the begining of the 'application' in seconds.
 */
NI._clockStart = undefined;
NI.timerInSeconds = function() {
  if (NI._clockStart === undefined) {
    NI._clockStart = Date.now();
  }
  return (Date.now() - NI._clockStart) / 1000.0;
};

function timestampInMs() {
  return Date.now();
}
NI.timestampInMs = timestampInMs;

function timestampInSeconds() {
  return Math.floor(Date.now() / 1000.0);
}
NI.timestampInSeconds = timestampInSeconds;

/**
 * Clone a JavaScript object.
 * @param {Object} obj
 * @param {boolean=} aShallowClone
 * @return {Object}
 */
NI.clone = function(obj, aShallowClone)  {
  // Handle the 3 simple types, and null or undefined
  if (null == obj || "object" != typeof obj) {
    return obj;
  }

  var copy, i, len, attr;

  // Handle Date
  if (obj instanceof Date) {
    copy = new Date();
    copy.setTime(obj.getTime());
    return copy;
  }

  // Handle Array
  if (obj instanceof Array) {
    copy = [];
    if (aShallowClone) {
      for (i = 0, len = obj.length; i < len; ++i) {
        copy[i] = obj[i];
      }
    }
    else {
      for (i = 0, len = obj.length; i < len; ++i) {
        copy[i] = NI.clone(obj[i]);
      }
    }
    return copy;
  }

  // Handle Object and objs with null prototype
  if (obj instanceof Object ||
      (typeof obj === 'object' && Object.prototype.toString.call(obj) === '[object Object]')) {
    copy = {};
    if (aShallowClone) {
      for (attr in obj) {
        if (hasProperty(obj,attr)) {
          copy[attr] = obj[attr];
        }
      }
    }
    else {
      for (attr in obj) {
        if (hasProperty(obj,attr)) {
          copy[attr] = NI.clone(obj[attr]);
        }
      }
    }
    return copy;
  }

  throw new Error("Unable to copy obj !");
};

/**
 * assignClone, deep clone the object and merge in the specified objects if any.
 * Objects 'on the right' overwrite values of the objects on the left.
 */
function assignDeepClone(target /*, source*/) {
  target = target || {};
  NI.assert.isObject(target);
  var from;
  var keys;
  var to = NI.clone(target,false);
  for (var s = 1; s < arguments.length; s++) {
    from = arguments[s];
    if (from) {
      NI.assert.isObject(from);
      keys = Object.keys(from);
      for (var i = 0; i < keys.length; ++i) {
        to[keys[i]] = NI.clone(from[keys[i]]);
      }
    }
  }
  return to;
};
exports.assignClone = assignDeepClone;
exports.assignDeepClone = assignDeepClone;
exports.deepClone = assignDeepClone;

/**
 * assignClone, shallow clone the object and merge in the specified objects if any.
 * Objects 'on the right' overwrite values of the objects on the left.
 */
function assignShallowClone(target /*, source*/) {
  target = target || {};
  NI.assert.isObject(target);
  var from;
  var keys;
  var to = NI.clone(target,true);
  for (var s = 1; s < arguments.length; s++) {
    from = arguments[s];
    if (from) {
      NI.assert.isObject(from);
      keys = Object.keys(from);
      for (var i = 0; i < keys.length; ++i) {
        to[keys[i]] = NI.clone(from[keys[i]],true);
      }
    }
  }
  return to;
};
exports.assignShallowClone = assignShallowClone;
exports.shallowClone = assignShallowClone;

/**
 * Get the owned values of the specified object.
 */
function getObjectValues(object) {
  var values = [];
  for (var key in object) {
    if (!hasProperty(object,key)) {
      continue;
    }
    values.push(object[key]);
  }
  return values;
}
exports.getObjectValues = Object.values ? Object.values : getObjectValues;
exports.objectValues = exports.getObjectValues;

/**
 * Get the owned keys of the specified object.
 */
function getObjectKeys(object) {
  var keys = [];
  for (var key in object) {
    if (!hasProperty(object,key)) {
      continue;
    }
    keys.push(key);
  }
  return keys;
}
exports.getObjectKeys = Object.keys ? Object.keys : getObjectKeys;
exports.objectKeys = exports.getObjectKeys;

/**
 * Gather the values in the specified object that match the specified array of keys.
 */
function gatherValues(object,aKeys) {
  var ret = [];
  _forEach(aKeys,function(v) {
    ret.push(object[v]);
  },undefined,false);
  return ret;
}
NI.gatherValues = gatherValues;

/**
 * Select n-levels deep into an object given a dot/bracket-notation query.
 * If partially applied, returns a function accepting the second argument.
 *
 * ### Examples:
 *
 *      selectn('name.first', contact);
 *
 *      selectn('addresses[0].street', contact);
 *
 *      contacts.map(selectn('name.first'));
 *
 * @param  {String} query
 * dot/bracket-notation query string
 *
 * @param  {Object} object
 * object to access
 *
 * @return {Function}
 * accessor function that accepts an object to be queried
 */
NI.selectn = function selectn(query) {
  var parts;

  // normalize query to `.property` access (i.e. `a.b[0]` becomes `a.b.0`)
  query = query.replace(/\[(\d+)\]/g, '.$1');
  parts = query.split('.') || [];

  /**
   * Accessor function that accepts an object to be queried
   *
   * @private
   *
   * @param  {Object} object
   * object to access
   *
   * @return {Mixed}
   * value at given reference or undefined if it does not exist
   */

  function accessor(object) {
    var ref = object || (1, eval)('this');
    var len = parts.length;
    var idx = 0;

    // iteratively save each segment's reference
    for (; idx < len; idx += 1) {
      if (ref) {
        ref = ref[parts[idx]];
      }
    }

    return ref;
  }

  // curry accessor function allowing partial application
  return arguments.length > 1
    ? accessor(arguments[1])
    : accessor;
};

/**
 * Check whether the specified value is a number.
 */
NI.isNumber = function(aValue) {
  return !isNaN(aValue);
};

/**
 * Check whether the specified value is NaN.
 */
NI.isNaN = isNaN;

/**
 * Round up the specified number
 */
function roundUp(num,div) {
  return num+((div-(num%div))%div)
}
NI.roundUp = roundUp;

/**
 * Check whether the specified value is 'strictly' NaN.
 */
NI.isStrictlyNaN = function(v) {
  return isNaN(v) && (v !== undefined);
};

if (!__BACKEND__) {
  /**
   * Add a global onload event handler.
   */
  NI.addOnLoad = function (aFunc) {
    if (window.attachEvent) {
      window.attachEvent('onload', aFunc);
    }
    else {
      if (window.onload) {
        var curronload = window.onload;
        var newonload = function(e) {
          curronload(e);
          aFunc();
        };
        window.onload = newonload;
      }
      else {
        window.onload = aFunc;
      }
    }
  };

  /**
   * Add a global onDOMContentLoaded event handler.
   */
  NI.addOnDOMLoaded = function(aFunc) {
    document.addEventListener('DOMContentLoaded', aFunc);
  }

  // Fix missing window.location.origin (IE...)
  if (!window.location.origin) {
    window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
  }
}

/**
 * Check whether the specified object can be considered "array-like"
 *
 * @return the value if it is array like, else undefined.
 */
function isArrayLike(object) {
  // the test order is important, object[0] allows using Immutable.js structure seamlessly here
  if (object && object[0] != undefined && (typeof(object.length) == "number")) {
    return object;
  }
  return undefined;
};
exports.isArrayLike = isArrayLike;

/**
 * Check whether the specified object is an array.
 *
 * @return the value if it is array like, else undefined.
 */
function isArray(o) {
  if (Object.prototype.toString.call(o) === '[object Array]') {
    return o;
  }
  return undefined;
};
exports.isArray = isArray;

/**
 * Check whether the specified value is a function.
 *
 * @return the value if it is a function, else undefined.
 */
function isFunction(aValue) {
  if (aValue && aValue.constructor && aValue.call && aValue.apply) {
    return aValue;
  }
  return undefined;
};
exports.isFunction = isFunction;

/**
 * Call if the value is a function, else return the value directly.
 */
function callOrGet(aValue) {
  if (isFunction(aValue)) {
    return aValue();
  }
  else {
    return aValue;
  }
}

/**
 * Check whether the specified value is a promise.
 *
 * @return the value if it is a promise, else undefined.
 */
function isPromise(aValue) {
  if (aValue && isFunction(aValue.then)) {
    return aValue;
  }
  return undefined;
};
exports.isPromise = isPromise;

/**
 * @private
 * @param {Array|Object|string} object
 * @param {function(*,*,(Array|Object|string))} block
 * @param {Array|Object|undefined} context
 * @param {boolean} abReversed
 * @return {Array|Object|string}
 */
var _forEach = function(object, block, context, abReversed) {
  if (object) {
    var i, len, key;
    if (object instanceof Function) {
      // functions have a "length" property
      for (key in object) {
        if (typeof object.prototype[key] == "undefined") {
          if (block.call(context, object[key], key, object) === false) {
            break;
          }
        }
      }
    }
    else if (typeof object == "string") {
      // the object is a string
      var str = /** @type {string} */ object;
      len = str.length;
      if (abReversed) {
        for (i = len-1; i >= 0; --i) {
          if (block.call(context, str.charCodeAt(i), i, str) === false) {
            break;
          }
        }
      }
      else {
        for (i = 0; i < len; ++i) {
          if (block.call(context, str.charCodeAt(i), i, str) === false) {
            break;
          }
        }
      }
    }
    else if (NI.isArrayLike(object)) {
      // the object is array-like
      len = object.size || object.length;
      if (abReversed) {
        for (i = len-1; i >= 0; --i) {
          if (block.call(context, object[i], i, object) === false) {
            break;
          }
        }
      }
      else {
        for (i = 0; i < len; ++i) {
          if (block.call(context, object[i], i, object) === false) {
            break;
          }
        }
      }
    }
    else {
      for (key in object) {
        if (hasProperty(object,key)) {
          if (block.call(context, object[key], key, object) === false) {
            break;
          }
        }
      }
    }
  }
  return object;
};

/**
 * Generic foreach
 * @param {Array|Object|String} object
 * @param {function(*,*,(Array|Object|string))} block
 * @param {Array|Object=} context
 * @return {Array|Object|string}
 */
function forEach(object, block, context) {
  return _forEach(object,block,context,false);
};
exports.forEach = forEach;

/**
 * Generic map
 * @param {Array|Object|String} object
 * @param {function(*,*,(Array|Object|string))} block
 * @param {Array|Object=} context
 * @return {Array}
 */
function map(object, block, context) {
  var r = [];
  _forEach(object,function(v,k,o) {
    r.push(block ? block(v,k,o) : v);
  },context,false);
  return r;
};
exports.map = map;

/**
 * Generic map, insert a separator before all elements except the first one.
 * @param {Array|Object|String} object
 * @param {Array|Object|String} separator
 * @param {function(*,*,(Array|Object|string))} block
 * @param {Array|Object=} context
 * @return {Array}
 */
function mapWithSeparator(object, separator, block, context) {
  var r = [];
  var isFirst = true;
  _forEach(object,function(v,k,o) {
    if (isFirst) {
      isFirst = false;
    }
    else {
      r.push(separator);
    }
    r.push(block ? block(v,k,o) : v);
  },context,false);
  return r;
};
exports.mapWithSeparator = mapWithSeparator;

/**
 * Generic filter. Returns all values which are not === undefined.
 * @param {Array|Object|String} object
 * @param {function(*,*,(Array|Object|string))} block
 * @param {Array|Object=} context
 * @return {Array}
 */
function filter(object, block, context) {
  var r = [];
  _forEach(object,function(v,k,o) {
    var pred = block ? block(v,k,o) : v;
    if (pred !== undefined) {
      r.push(pred);
    }
  },context,false);
  return r;
};
exports.filter = filter;

/**
 * Generic filter, insert a separator before all elements except the first
 * one. Returns all values which are not === undefined.
 * @param {Array|Object|String} object
 * @param {function(*,*,(Array|Object|string))} block
 * @param {Array|Object=} context
 * @return {Array}
 */
function filterWithSeparator(object, separator, block, context) {
  var r = [];
  var isFirst = true;
  _forEach(object,function(v,k,o) {
    var pred = block ? block(v,k,o) : v;
    if (pred !== undefined) {
      if (isFirst) {
        isFirst = false;
      }
      else {
        r.push(separator);
      }
      r.push(pred);
    }
  },context,false);
  return r;
};
exports.filterWithSeparator = filterWithSeparator;

/**
 * Generic foldl
 * @param {Array|Object|String} object
 * @param {function(*,*,(Array|Object|string))} block
 * @param {Array|Object=} context
 * @return {Array}
 */
function foldl(object, start, block, context) {
  var r = start;
  _forEach(object,function(v,k,o) {
    r = block(r, v, k, o);
  },context,false);
  return r;
};
exports.foldl = foldl;

/**
 * Generic sum
 */
function sum(object,key) {
  return foldl(object,0,function(r,k) { return r + k[key] })
}
exports.sum = sum;

/**
 * Returns the first item for which the predicate return !== undefined.
 * @param {Array|Object|String} object
 * @param {function(*,*,(Array|Object|string))} block
 * @param {Array|Object=} context
 * @return {Array|Object|String}
 */
function find(object, block, context) {
  var r;
  _forEach(object,function(v,k,o) {
    var pred = block(v,k,o);
    if (pred !== undefined) {
      r = pred;
      return false;
    }
    return true;
  },context,false);
  return r;
};
exports.find = find;

// Is a given variable an object?
function isObject(obj) {
  var type = typeof obj;
  return type === 'function' || type === 'object' && !!obj;
};
exports.isObject = isObject;

// Is a given variable an object literal?
function isObjectLiteral(obj) {
  var type = typeof obj;
  return type === 'object' && !!obj && obj.constructor === {}.constructor;
};
exports.isObjectLiteral = isObjectLiteral;

/**
 * Check whether the property or index B is in the object/array A.
 */
function has (A,B) {
  return ((typeof A === "object") && (B in A));
};
exports.has = has;

/**
 * Check whether the property or index B is in the object/array A and its value is defined.
 */
function hasDefined (A,B) {
  return ((typeof A === "object") && (B in A) && (A[B] !== undefined));
};
exports.hasDefined = hasDefined;

/**
 * Check whether the property or index B is in the object/array A and its value is a function.
 */
function hasFunction (A,B) {
  return ((typeof A === "object") && (B in A) && NI.isFunction(A[B]));
};
exports.hasFunction = hasFunction;

// Add some isType methods: isArguments, isString, isDate, isRegExp, isError.
NI.forEach(['Arguments', 'String', 'Number', 'Date', 'RegExp', 'Error'], function(name) {
  exports['is' + name] = function(obj) {
    return Object.prototype.toString.call(obj) === '[object ' + name + ']';
  };
});

/**
 * Check if object has own property
 */
var _hasOwnProperty = Object.prototype.hasOwnProperty;

function hasProperty(obj, key) {
  if (!obj) {
    return false;
  }
  return _hasOwnProperty.call(obj,key);
}
exports.hasProperty = hasProperty;

/**
 * Check whether the specified object is empty.
 */
function isEmpty(obj) {
  if (!obj) {
    return true;
  }
  if (typeof obj === "string") {
    return obj.length == 0;
  }
  else {
    for (var key in obj) {
      if (hasProperty(obj, key)) {
        return false;
      }
    }
  }
  return true;
};
exports.isEmpty = isEmpty;
exports.empty = isEmpty;

/**
 * Check whether the specified object is *not* empty.
 */
function isNotEmpty(obj) {
  return !isEmpty(obj);
}
exports.isNotEmpty = isNotEmpty;
exports.notEmpty = isNotEmpty;

/**
 * Return the number of items in an object/array/string.
 */
function size(obj) {
  if (!obj) {
    return 0;
  }
  if (obj.length !== undefined) {
    return obj.length;
  }
  var count = 0;
  for (var key in obj) {
    if (hasProperty(obj,key)) {
      ++count;
    }
  }
  return count;
};
exports.size = size;

/**
 * Return the first item in an object/array/string
 */
function first(obj) {
  if (!obj) {
    return undefined;
  }
  if (obj.length !== undefined) {
    return obj[0];
  }
  for (var key in obj) {
    if (hasProperty(obj,key)) {
      return obj[key];
    }
  }
  return undefined;
};
exports.first = first;

/**
 * Return the last item in an object/array/string
 */
function last(obj) {
  if (!obj) {
    return undefined;
  }
  if (obj.length !== undefined) {
    return obj.length > 0 ? obj[obj.length-1] : undefined;
  }
  var lastItem = undefined;
  for (var key in obj) {
    if (hasProperty(obj,key)) {
      lastItem = obj[key];
    }
  }
  return lastItem;
};
exports.last = last;

/**
 * Generic reverseForEach
 * @param {Array|Object|String} object
 * @param {function(*,*,(Array|Object|string))} block
 * @param {Array|Object=} context
 * @return {Array|Object|string}
 */
function reverseForEach(object, block, context) {
  return _forEach(object,block,context,true);
};
exports.reverseForEach = reverseForEach;

/**
 * Create an array from a range of integer.
 */
function arrayFromIntRange(aStart,aEnd) {
  var array = [];
  if (aStart > aEnd) {
    throw new Error("Start must be smaller than the end !");
  }
  for (var i = aStart; i < aEnd; ++i) {
    array.push(i);
  }
  return array;
};
exports.arrayFromIntRange = arrayFromIntRange;

/**
 * Shuffle an array.
 */
function arrayShuffle(array) {
  var tmp, current, top = array.length;
  if (top) {
    while(--top) {
      current = Math.floor(Math.random() * (top + 1));
      tmp = array[current];
      array[current] = array[top];
      array[top] = tmp;
    }
  }
  return array;
};
exports.arrayShuffle = arrayShuffle;

/**
 * Pick N random items from the specified array.
 */
function arrayPickNRandom(aArray,aCount,aCanPickPredicate) {
  var array = NI.clone(aArray);
  var ret = [];
  var numToRemove = Math.min(aCount,array.length);
  for (var i = 0; i < numToRemove; ++i) {
    var index = Math.min((Math.random()*array.length)>>>0,array.length-1);
    var item = array[index];
    NI.arrayRemove(array,index);
    if (!aCanPickPredicate || aCanPickPredicate(item)) {
      ret.push(item);
    }
  }
  return ret;
};
exports.arrayPickNRandom = arrayPickNRandom;

/**
 * Remove one or more items from an array.
 */
function arrayRemove(aArray,aStartIndex,aEndIndex) {
  if (aArray === undefined) {
    return [];
  }
  if (aStartIndex === undefined && aEndIndex === undefined) {
    return aArray;
  }
  var rest = aArray.slice((aEndIndex || aStartIndex) + 1 || aArray.length);
  aArray.length = aStartIndex < 0 ? aArray.length + aStartIndex : aStartIndex;
  return aArray.push.apply(this, rest) || [];
};
exports.arrayRemove = arrayRemove;

/**
 * Check whether the specified array already contains the specified object.
 */
function arrayContains(array,item) {
  var len = array.length;
  for (var i = 0; i < len; ++i) {
    if (array[i] == item) {
      return true;
    }
  }
  return undefined;
};
exports.arrayContains = arrayContains;

/**
 * Return the index at which the specified item is found. Returns undefined if the item can't be found.
 */
function arrayFind(array,item) {
  if (item === undefined) {
    return undefined;
  }
  var len = array.length;
  for (var i = 0; i < len; ++i) {
    if (array[i] == item) {
      return i;
    }
  }
  return undefined;
};
exports.arrayFind = arrayFind;

/**
 * Push an item in the array, makes sure the item isn't already in the array.
 */
function arrayPushOnce(array,item) {
  if (!NI.arrayContains(array,item)) {
    array.push(item);
  }
  return array;
};
exports.arrayPushOnce = arrayPushOnce;

/**
 * Compare two arrays, C-like, returns 0 if equal, -1 if A is "before" B, 1 if
 * A "after" B.
 * @param {Array} A
 * @param {Array} B
 * @param {number=} num
 */
function arrayCmp(A, B, num) {
  // Take the max length... if not the same size the shorter array will return
  // undefined and the value in the other will just be compared with
  // undefined... which is maybe not 100% perfect, but is fine for most proper
  // apps - aka which feed proper input.
  num = num || Math.max(A.length,B.length);
  for (var i = 0; i < num; ++i) {
    var v1 = A[i];
    var v2 = B[i];
    if (v1 != v2) {
      return v1 > v2 ? 1 : -1;
    }
  }
  return 0;
};
exports.arrayCmp = arrayCmp;

/**
 * Set every element in the array to the specifed value.
 */
function arrayFill(A, aValue) {
  var len = A.length;
  for (var i = 0; i < len; ++i) {
    A[i] = aValue;
  }
  return 0;
};
exports.arrayFill = arrayFill;

/**
 * Sort an array inplace using the specified key.
 */
function arraySort(arr, key, dir) {
  NI.assert.isArray(arr);

  if (!key) {
    // no valid key, just return the array
    return arr;
  }

  if (NI.isFunction(key)) {
    // key is a regular predicate function
    return arr.sort(key);
  }

  dir = dir || 1;
  if ((typeof key === "string") && (key.indexOf('.') >= 0 || key.indexOf('[') >= 0)) {
    // key is a complex selector, contains [] or .
    var selector = NI.selectn(key);
    return arr.sort(function(a,b) {
      var valA = selector(a);
      var valB = selector(b);
      return (valA < valB) ? -dir : (valA > valB) ? dir : 0;
    });
  }
  else {
    // key is a simple index or key name
    return arr.sort(function(a,b) {
      var valA = a[key];
      var valB = b[key];
      return (valA < valB) ? -dir : (valA > valB) ? dir : 0;
    });
  }
}
exports.arraySort = arraySort;

/**
 * Stable sort an array - returns a new sorted array.
 */
function arrayStableSort(arr, key, dir) {
  NI.assert.isArray(arr);

  // no key, value based sorting
  if (!key) {
    return arrayStableSort_doSort(arr, function(left, right) {
      if (left < right) {
        return -1;
      }
      if (left == right) {
        return 0;
      }
      else {
        return 1;
      }
    });
  }
  // key is a regular predicate function
  else if (NI.isFunction(key)) {
    return arrayStableSort_doSort(arr, key);
  }
  // key is a property name
  else {
    dir = dir || 1;
    if ((typeof key === "string") && (key.indexOf('.') >= 0 || key.indexOf('[') >= 0)) {
      // key is a complex selector, contains [] or .
      var selector = NI.selectn(key);
      return arrayStableSort_doSort(arr, function(a,b) {
        var valA = selector(a);
        var valB = selector(b);
        return (valA < valB) ? -dir : (valA > valB) ? dir : 0;
      });
    }
    else {
      // key is a simple index or key name
      return arrayStableSort_doSort(arr, function(a,b) {
        var valA = a[key];
        var valB = b[key];
        return (valA < valB) ? -dir : (valA > valB) ? dir : 0;
      });
    }
  }
}
exports.arrayStableSort = arrayStableSort;

function arrayStableSort_merge(left, right, compare) {
  var result = [];

  while (left.length > 0 || right.length > 0) {
    if (left.length > 0 && right.length > 0) {
      if (compare(left[0], right[0]) <= 0) {
        result.push(left[0]);
        left = left.slice(1);
      }
      else {
        result.push(right[0]);
        right = right.slice(1);
      }
    }
    else if (left.length > 0) {
      result.push(left[0]);
      left = left.slice(1);
    }
    else if (right.length > 0) {
      result.push(right[0]);
      right = right.slice(1);
    }
  }
  return result;
}

function arrayStableSort_doSort(aArray,aCmp) {
  var length = aArray.length,
      middle = Math.floor(length / 2);

  if (length < 2) {
    return aArray;
  }

  return arrayStableSort_merge(
    arrayStableSort_doSort(aArray.slice(0, middle),aCmp),
    arrayStableSort_doSort(aArray.slice(middle, length),aCmp),
    aCmp
  );
}

/**
 * Slices array into chunks, or invoke a function with each slice.
 */
function arraySliceBy(aArray, aSize, aFunc) {
  var offset = 0;
  var slices = [];

  while(offset < aArray.length) {
    var slice = aArray.slice(offset, offset+aSize);

    if(NI.isFunction(aFunc)) {
      aFunc(slice);
    } else {
      slices.push(slice);
    }

    offset += aSize;
  }

  if (!NI.isFunction(aFunc)) {
    return slices
  }
  else {
    return undefined;
  }
}
exports.arraySliceBy = arraySliceBy;

/**
 * Consistent object type, returned in lower case.
 */
function objectType(obj) {
  if (typeof obj === "undefined") {
    return "undefined";
    // consider: typeof null === object
  }
  if (obj === null) {
    return "null";
  }

  var type = Object.prototype.toString.call( obj ).match(/^\[object\s(.*)\]$/)[1] || "";
  switch ( type ) {
  case "Number":
    if ( isNaN(obj) ) {
      return "nan";
    }
    return "number";
  case "String":
  case "Boolean":
  case "Array":
  case "Date":
  case "RegExp":
  case "Function":
    return type.toLowerCase();
  }
  if (typeof obj === "object") {
    return "object";
  }
  return undefined;
};
exports.objectType = objectType;

/**
 * Wrap into an array if it isn't already array like.
 *
 * This is usefull to use with foreach, when we don't want to iterate over
 * object properties.
 */
function boxInArray (v) {
  if (NI.isArrayLike(v)) {
    return v;
  }
  return [v];
};
exports.boxInArray = boxInArray;

function jsonDecycle(object,aCanDecyclePredicate) {
  'use strict';

  // Make a deep copy of an object or array, assuring that there is at most
  // one instance of each object or array in the resulting structure. The
  // duplicate references (which might be forming cycles) are replaced with
  // an object of the form
  //      {$ref: PATH}
  // where the PATH is a JSONPath string that locates the first occurance.
  // So,
  //      var a = [];
  //      a[0] = a;
  //      return JSON.stringify(JSON.decycle(a));
  // produces the string '[{"$ref":"$"}]'.

  // JSONPath is used to locate the unique object. $ indicates the top level of
  // the object or array. [NUMBER] or [STRING] indicates a child member or
  // property.

  var objects = [],   // Keep a reference to each unique object or array
      paths = [];     // Keep the path to each unique object or array

  return (function derez(value, path) {

    // The derez recurses through the object, producing the deep copy.

    var i,          // The loop counter
        name,       // Property name
        nu;         // The new object or array

    // typeof null === 'object', so go on if this value is really an object but not
    // one of the weird builtin objects.

    if (typeof value === 'object' &&
        value !== null &&
        (!aCanDecyclePredicate ||
         aCanDecyclePredicate(value)) &&
        !(value instanceof Boolean) &&
        !(value instanceof Date) &&
        !(value instanceof Number) &&
        !(value instanceof RegExp) &&
        !(value instanceof String)
       )
    {
      // If the value is an object or array, look to see if we have already
      // encountered it. If so, return a $ref/path object. This is a hard way,
      // linear search that will get slower as the number of unique objects grows.

      for (i = 0; i < objects.length; i += 1) {
        if (objects[i] === value) {
          return {$ref: paths[i]};
        }
      }

      // Otherwise, accumulate the unique value and its path.

      objects.push(value);
      paths.push(path);

      // If it is an array, replicate the array.

      if (Object.prototype.toString.apply(value) === '[object Array]') {
        nu = [];
        for (i = 0; i < value.length; i += 1) {
          nu[i] = derez(value[i], path + '[' + i + ']');
        }
      } else {

        // If it is an object, replicate the object.

        nu = {};
        for (name in value) {
          if (hasProperty(value, name)) {
            nu[name] = derez(value[name],
                             path + '[' + JSON.stringify(name) + ']');
          }
        }
      }
      return nu;
    }
    return value;
  }(object, '$'));
};
exports.jsonDecycle = jsonDecycle;

function jsonRetrocycle($) {
  'use strict';

  // Restore an object that was reduced by decycle. Members whose values are
  // objects of the form
  //      {$ref: PATH}
  // are replaced with references to the value found by the PATH. This will
  // restore cycles. The object will be mutated.

  // The eval function is used to locate the values described by a PATH. The
  // root object is kept in a $ variable. A regular expression is used to
  // assure that the PATH is extremely well formed. The regexp contains nested
  // * quantifiers. That has been known to have extremely bad performance
  // problems on some browsers for very long strings. A PATH is expected to be
  // reasonably short. A PATH is allowed to belong to a very restricted subset of
  // Goessner's JSONPath.

  // So,
  //      var s = '[{"$ref":"$"}]';
  //      return JSON.retrocycle(JSON.parse(s));
  // produces an array containing a single element which is the array itself.

  var px = /^\$(?:\[(?:\d+|\"(?:[^\\\"\u0000-\u001f]|\\([\\\"\/bfnrt]|u[0-9a-zA-Z]{4}))*\")\])*$/;

  (function rez(value) {

    // The rez function walks recursively through the object looking for $ref
    // properties. When it finds one that has a value that is a path, then it
    // replaces the $ref object with a reference to the value that is found by
    // the path.

    var i, item, name, path;

    if (value && typeof value === 'object') {
      if (Object.prototype.toString.apply(value) === '[object Array]') {
        for (i = 0; i < value.length; i += 1) {
          item = value[i];
          if (item && typeof item === 'object') {
            path = item.$ref;
            if (typeof path === 'string' && px.test(path)) {
              value[i] = eval(path); // eslint-disable-line
            } else {
              rez(item);
            }
          }
        }
      } else {
        for (name in value) {
          if (typeof value[name] === 'object') {
            item = value[name];
            if (item) {
              path = item.$ref;
              if (typeof path === 'string' && px.test(path)) {
                value[name] = eval(path); // eslint-disable-line
              } else {
                rez(item);
              }
            }
          }
        }
      }
    }
  }($));
  return $;
};
exports.jsonRetrocycle = jsonRetrocycle;

var isValidIdentifier = function() {
  var validName = /^[$A-Z_][0-9A-Z_$]*$/i;
  return function(s) {
    // Ensure a valid name and not reserved.
    return validName.test(s);
  };
}();
exports.isValidIdentifier = isValidIdentifier;

function callObjectToDebugString(aValue) {
  if (typeof aValue === 'object') {
    var toString = aValue.toDebugString;
    if (typeof toString !== "function") {
      toString = aValue.format;
    }
    if (typeof toString !== "function") {
      toString = aValue.toString;
    }
    if (typeof toString === "function") {
      var toStringRet = toString.call(aValue);
      if (typeof toStringRet === "string" &&
          toStringRet[0] != "[")
      {
        // if toString returns a string that isn't [object Somthing]
        return toStringRet;
      }
    }
  }
  return undefined;
}

function toDebugStringCanDecyclePredicate(aValue) {
  if (typeof callObjectToDebugString(aValue) === "string") {
    return false;
  }
  else {
    return true;
  }
}

/**
 * Sensible conversion to a string for debugging.
 *
 * @param {*} aValue
 * @param {string=} aSpace
 * @param {function(*,*)=} aReplacer
 */
function toDebugString(aValue, aSpace, aIndentation, aIsNotRoot) {
  aIndentation = aIndentation || '';
  aSpace = aSpace || '';
  var r = "";
  var type = objectType(aValue);
  switch (type) {
  case "function": {
    if (aValue._className) {
      r = "class " + aValue._className + " ";
      var proto = jsonDecycle(aValue.prototype);
      var c = proto.constructor
      proto.constructor = function() {}; // break the cycle
      r += toDebugString(proto, aSpace, aIndentation, aIsNotRoot);
      c.constructor = c;
    }
    else {
      var displayName = aValue.displayName || aValue.name;
      if (displayName) {
        r = displayName;
      }
      else {
        r = (aValue._funcName || "function") + '()';
      }
    }
    break;
  }
  case "array":
  case "object": {
    try {
      if (type != "array") {
        var toStringRet = callObjectToDebugString(aValue)
        if (toStringRet) {
          return '/*obj*/"'+toStringRet+'"';
        }
      }

      if (!aIsNotRoot) {
        aValue = jsonDecycle(
          aValue, toDebugStringCanDecyclePredicate);
      }

      var singleLineVal;
      if (isObject(aValue) && isShallowObject(aValue)) {
        singleLineVal = objectToDebugString(aValue, '', '', true);
        if (singleLineVal && singleLineVal.length < toDebugString.maxSingleLineLength) {
          return singleLineVal;
        }
      }

      r = objectToDebugString(
        aValue, aSpace, aIndentation, true)
    }
    catch (e) {
      NI.log("??? " + e.stack);
      r = "<<ERROR: exc = "+e+", value = "+aValue+">>";
    }
    break;
  }
  case "string": {
    r = "'"+aValue+"'";
    break;
  }
  default: {
    r = ""+aValue;
    break;
  }
  }
  return r;
};
exports.toDebugString = toDebugString;
toDebugString.maxSingleLineLength = 100;

function objectToDebugString(aValue, aSpace, aIndentation) {
  aIndentation = aIndentation || '';
  aSpace = aSpace || '';
  var r = "";
  var type = objectType(aValue);
  try {
    var bIsArray = (type === "array");
    var numProps = size(aValue);
    var doNewLine = (aSpace && aSpace.length > 0);
    r += (bIsArray?"[":"{");
    // r += "/*" + numProps + "*/";
    if (doNewLine) {
      r += "\n";
    }
    else {
      r += " ";
    }
    var currentIndex = 0;
    var propIndent = doNewLine ? (aIndentation + aSpace) : '';
    forEach(aValue, function(aVal,aKey) {
      r += propIndent;
      if (!bIsArray) {
        if (!isValidIdentifier(aKey)) {
          r += "\"" + aKey + "\": ";
        }
        else {
          r += aKey + ": ";
        }
      }
      else {
        var valType = objectType(aVal);
        if (valType === "array" ||
            valType === "object") {
          r += "/*[" + aKey + "]*/";
        }
      }

      r += toDebugString(aVal, aSpace, (propIndent + aSpace), true, aKey);
      if (currentIndex != (numProps-1)) {
        r += ","
      }
      if (doNewLine) {
        r += "\n";
      }
      else {
        r += " ";
      }
      ++currentIndex;
    });
    r += aIndentation.slice(0,-aSpace.length) + (bIsArray?"]":"}");
  }
  catch (e) {
    NI.log("??? " + e.stack);
    r = "<<ERROR: exc = "+e+", value = "+aValue+">>";
  }
  return r;
}

function isShallowObject(obj) {
  NI.forEach(obj,function(val) {
    if (typeof val === "object") {
      return false;
    }
    else if (typeof val === "function") {
      return false;
    }
    return true;
  });
  return true;
}
exports.isShallowObject = isShallowObject;

/**
 * Check whether {str} starts with specified {prefix}
 */
function stringStartsWith(str, prefix) {
  if ((typeof str) !== "string") {
    return undefined;
  }
  return str.indexOf(prefix) == 0;
};
exports.stringStartsWith = stringStartsWith;

/**
 * Check whether {str} ends with specified {suffix}
 */
function stringEndsWith(str, suffix) {
  if ((typeof str) !== "string") {
    return undefined;
  }
  return str.indexOf(suffix, str.length - suffix.length) !== -1;
};
exports.stringEndsWith = stringEndsWith;

/**
 * Check whether {str} contains {substr}
 */
function stringContains(str, substr) {
  if ((typeof str) !== "string") {
    return undefined;
  }
  return str.indexOf(substr) != -1;
};
exports.stringContains = stringContains;

/**
 * Return the string in {str} before {substr}
 */
function stringBefore(str, substr) {
  return str.substring(0, str.indexOf(substr));
};
exports.stringBefore = stringBefore;

/**
 * Return the string in {str} before {substr} starting from the end of the string
 */
function stringRBefore(str, substr) {
  return str.substring(0, str.lastIndexOf(substr));
};
exports.stringRBefore = stringRBefore;

/**
 * Return the string in {str} after {substr}
 */
function stringAfter(str, substr) {
  var pos = str.indexOf(substr);
  if (pos < 0) {
    return "";
  }
  pos += substr.length;
  return str.substring(pos, str.length);
};
exports.stringAfter = stringAfter;

/**
 * Return the string in {str} after {substr} starting from the end of the string
 */
function stringRAfter(str, substr) {
  var pos = str.lastIndexOf(substr);
  if (pos < 0) {
    return "";
  }
  pos += substr.length;
  return str.substring(pos, str.length);
};
exports.stringRAfter = stringRAfter;

/**
 * Trims whitespace from the beginning and end of the string.
 */
function stringTrim(aStr) {
  return aStr.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
};
exports.stringTrim = stringTrim;

/**
 * Trims whitespace from the left side of the string.
 */
function stringTrimLeft(aStr) {
  return aStr.replace(/^\s+/,'');
};
exports.stringTrimLeft = stringTrimLeft;

/**
 * Trims whitespace from the right side of the string.
 */
function stringTrimRight(aStr) {
  return aStr.replace(/\s+$/,'');
};
exports.stringTrimRight = stringTrimRight;

/**
 * Converts a hexadecimal string to a byte.
 */
function stringHex2Byte(aStr) {
  return parseInt(aStr,16);
};
exports.stringHex2Byte = stringHex2Byte;

/**
 * Converts a byte to an hexadecimal string, the returned string is always 2 characters long.
 */
function stringByte2Hex(aInt) {
  var r = aInt.toString(16);
  if (r.length == 1) {
    return "0" + r;
  }
  else if (r.length == 0) {
    return "00";
  }
  else {
    return r;
  }
};
exports.stringByte2Hex = stringByte2Hex;

/**
 * Converts a byte to an bits string, the returned string is always 8 characters long.
 */
function stringByte2Bits(aInt) {
  var r = aInt.toString(2);
  for (var i = r.length; i < 8; ++i) {
    r = "0" + r;
  }
  return r;
};
exports.stringByte2Bits = stringByte2Bits;

/**
 * Converts an hexadecimal string to an array of byte.
 */
function stringHexString2ByteArray(aStr) {
  var bytes = [];
  var len = aStr.length;
  for (var i = 0; i < len; i += 2) {
    bytes.push(NI.stringHex2Byte(aStr.substr(i,2)));
  }
  return bytes;
};
exports.stringHexString2ByteArray = stringHexString2ByteArray;

/**
 * Converts an array of byte to an hexadecimal string.
 */
function stringByteArray2HexString(aBytes) {
  var r = "";
  NI.forEach(aBytes, function(aByte) {
    r += NI.stringByte2Hex(aByte);
  });
  return r;
};
exports.stringByteArray2HexString = stringByteArray2HexString;

/**
 * Generate a string filled with random bytes.
 */
function stringRandomBytes(aLen) {
  var r = "";
  for (var i = 0; i < aLen; ++i) {
    var b = ((Math.random()*256)&0xFF) >>> 0;
    r += String.fromCharCode(b);
  }
  return r;
};
exports.stringRandomBytes = stringRandomBytes;

/**
 * Generate a string of random bytes as an hexadecimal string. The string
 * returned will be of length = aLen * 2.
 */
function stringRandomBytesAsHex(aLen) {
  return NI.stringByteArray2HexString(
    NI.randomBytes(aLen));
};
exports.stringRandomBytesAsHex = stringRandomBytesAsHex;

/**
 * Camelize a dashed-property-name.
 */
function stringCamelize(str) {
  return str.replace(/-+(.)?/g, function(match, chr) {
    return chr ? chr.toUpperCase() : '';
  });
};
exports.stringCamelize = stringCamelize;

/**
 * Dasherize a camelCasedPropertyName.
 */
function stringDasherize(str) {
  return str.replace(/::/g, '/')
    .replace(/([A-Z]+)([A-Z][a-z])/g, '$1_$2')
    .replace(/([a-z\d])([A-Z])/g, '$1_$2')
    .replace(/_/g, '-')
    .toLowerCase();
};
exports.stringDasherize = stringDasherize;

/**
 * Spaceize a camelCasedPropertyName.
 */
function stringSpaceize(str,aSpaceChar) {
  return str.replace(/::/g, '/')
    .replace(/([A-Z]+)([A-Z][a-z])/g, '$1_$2')
    .replace(/([a-z\d])([A-Z])/g, '$1_$2')
    .replace(/_/g, aSpaceChar || ' ');
};
exports.stringSpaceize = stringSpaceize;

/**
 * Converts a date object to a standard date: YYYY/MM/DD-HH:MM:SS
 */
function dateToString (date, dateSep, partSep, hourSep) {
  if (typeof date === "number") {
    // assume this is a unix timestamp
    date = new Date(date);
  }
  else if (typeof date === "string") {
    date = new Date(date);
  }
  else if (!NI.isDate(date)) {
    return "<<invalid date object>>";
  }
  var yyyy = date.getFullYear().toString();
  var mm  = (date.getMonth()+1).toString(); // getMonth() is zero-based
  var dd  = date.getDate().toString();
  var HH  = date.getHours().toString();
  var MM  = date.getMinutes().toString();
  var SS  = date.getSeconds().toString();
  dateSep = dateSep || "/";
  partSep = partSep || "-";
  hourSep = hourSep || ":";
  return yyyy + dateSep + (mm[1]?mm:"0"+mm[0]) + dateSep + (dd[1]?dd:"0"+dd[0]) + partSep +
    (HH[1]?HH:"0"+HH[0]) + hourSep + (MM[1]?MM:"0"+MM[0]) + hourSep + (SS[1]?SS:"0"+SS[0]);
};
exports.dateToString = dateToString;

/**
 * Escapes the specified string.
 */
function stringEscape (val) {
  if (typeof(val) != "string") {
    return val;
  }
  return val
    .replace(/[\\]/g, '\\\\')
    .replace(/[\/]/g, '\\/')
    .replace(/[\b]/g, '\\b')
    .replace(/[\f]/g, '\\f')
    .replace(/[\n]/g, '\\n')
    .replace(/[\r]/g, '\\r')
    .replace(/[\t]/g, '\\t')
    .replace(/[\"]/g, '\\"')
    .replace(/\\'/g, "\\'");
};
exports.stringEscape = stringEscape;

/**
 * Encode the specified string so that it can be embedded in a URI.
 */
function stringEncodeURI (str) {
  return str ? encodeURIComponent(str) : "";
};
exports.stringEncodeURI = stringEncodeURI;

/**
 * Decode the specified string from a URI encoded string.
 */
function stringDecodeURI (str) {
  return str ? decodeURIComponent(str) : "";
};
exports.stringDecodeURI = stringDecodeURI;

/**
 * Replace the specified ${variable} in the specified string.
 */
function stringReplaceVar(str, name, value) {
  // name can be a map of variables / values
  if (typeof(name) == "object") {
    NI.assert.isFalse(value);
    NI.forEach(name,function(aVal,aName) {
      NI.assert.isTypeOf(aName,"string");
      str = NI.stringReplaceVar(str,aName,aVal);
    });
    return str;
  }

  NI.assert.isDefined(str);
  // convert value to a string
  if (value === undefined) {
    throw ErrorFmt("value is undefined for parameter '"+name+"'.");
  }
  value = value.toString();
  // make sure the value isnt another variable, this would only lead to baaaad
  // times. (circular ref / infinite loop)
  NI.assert.isFalse(NI.stringStartsWith(value,"${"));

  var varname = "${" + name + "}";
  while (true) {
    var newstr = str.replace(varname, value);
    if (str == newstr) {
      break;
    }
    if (newstr) {
      str = newstr;
    }
  }

  return str;
};
exports.stringReplaceVar = stringReplaceVar;

function stringPadBefore(aString,aPadWith,aNewLen) {
  var s = aString;
  while (s.length < aNewLen) {
    s = aPadWith + s;
  }
  return s;
}
exports.stringPadBefore = stringPadBefore;

function stringPadAfter(aString,aPadWith,aNewLen) {
  var s = aString;
  while (s.length < aNewLen) {
    s = s + aPadWith;
  }
  return s;
}
exports.stringPadAfter = stringPadAfter;

/**
 * Split a string every N characters.
 */
function stringSplitNChars(aString,aLen) {
  var result = [];
  for (var i = 0; i < aString.length; i += aLen) {
    result.push(aString.substr(i, aLen));
  }
  return result;
}
exports.stringSplitNChars = stringSplitNChars;

/**
 * Convert the specified string to bytes in hexadecimal
 */
function string2Hex(aString) {
  return Buffer(aString).toString('hex');
}
exports.string2Hex = string2Hex;

/**
 * Convert the specified hexadecimal to a utf8 string
 */
function hex2String(aHex) {
  return Buffer(aHex, 'hex').toString('utf8');
}
exports.hex2String = hex2String;

/**
 * Insert the specified string every N characters.
 */
function stringInsertEveryN(aString,aNumChars,aToInsert) {
  var n = aNumChars || 4;
  return aString.match(new RegExp(/*".{"+n+"}(?=.{"+(n-1)+","+n+"})|.+"*/ ".{"+n+"}|.+","g")).join(aToInsert || "-");
}
exports.stringInsertEveryN = stringInsertEveryN;

function rainbow(step, numOfSteps) {
  // This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
  // Adam Cole, 2011-Sept-14
  // HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
  var r, g, b;
  var h = step / numOfSteps;
  var i = ~~(h * 6);
  var f = h * 6 - i;
  var q = 1 - f;
  switch(i % 6) {
  case 0: r = 1; g = f; b = 0; break;
  case 1: r = q; g = 1; b = 0; break;
  case 2: r = 0; g = 1; b = f; break;
  case 3: r = 0; g = q; b = 1; break;
  case 4: r = f; g = 0; b = 1; break;
  case 5: r = 1; g = 0; b = q; break;
  }
  var c = "#" + ("00" + (~ ~(r * 255)).toString(16)).slice(-2) + ("00" + (~ ~(g * 255)).toString(16)).slice(-2) + ("00" + (~ ~(b * 255)).toString(16)).slice(-2);
  return (c);
}
exports.rainbow = rainbow;

/**
 * Parse a boolean style value.
 */
function parseBool(aValue) {
  switch(aValue) {
  case 1: case '1':
  case true: case 'true': case 'TRUE':
  case 'on': case 'ON':
  case 'yes': case 'YES': {
    return true;
  }
  case 0: case '0':
  case false: case 'false': case 'FALSE':
  case 'off': case 'OFF':
  case 'no': case 'NO': {
    return false;
  }
  default: {
    return !!aValue;
  }
  }
};
exports.parseBool = parseBool;

function isBool(aValue) {
  switch(aValue) {
  case 1: case '1':
  case true: case 'true': case 'TRUE':
  case 'on': case 'ON':
  case 'yes': case 'YES':
  case 0: case '0':
  case false: case 'false': case 'FALSE':
  case 'off': case 'OFF':
  case 'no': case 'NO': {
    return true;
  }
  }
  return false;
};
exports.isBool = isBool;

/**
 * Parse an integer style value.
 */
function _parseInt(aValue) {
  if (aValue === undefined ||
      aValue === false ||
      aValue === null ||
      aValue === '')
  {
    return 0;
  }
  if (aValue === true)
  {
    return 1;
  }
  return parseInt(aValue);
};
exports.parseInt = _parseInt;

/**
 * Parse a float style value.
 */
function _parseFloat(aValue) {
  if (aValue === undefined ||
      aValue === false ||
      aValue === null ||
      aValue === '')
  {
    return 0.0;
  }
  if (aValue === true)
  {
    return 1.0;
  }
  return parseFloat(aValue);
};
exports.parseFloat = _parseFloat;

//======================================================================
// JSON
//======================================================================

// Check if the string has a high probablity of being actual an JSON object or array.
function maybeJson(astrJson) {
  if (typeof(astrJson) !== "string" || astrJson.length == 0) {
    return false;
  }
  var firstChar = astrJson[0];
  if (firstChar == "{") {
    if (astrJson[astrJson.length-1] == "}") {
      return true;
    }
  }
  else if (firstChar == "[") {
    if (astrJson[astrJson.length-1] == "]") {
      return true;
    }
  }
  return false;
}
exports.maybeJson = maybeJson;

function jsonParse(astrJson,aReturnIfError) {
  try {
    return JSON.parse(astrJson);
  }
  catch (e) {
    return aReturnIfError;
  }
};
exports.jsonParse = jsonParse;

// If astrJson is proper json parse it and returns the object encoded in the
// string. Otherwise returns the string itself or aReturnIfError if its
// specified.
function jsonParseMaybe(astrJson,aReturnIfError) {
  aReturnIfError = aReturnIfError || astrJson;
  if (!maybeJson(astrJson)) {
    return aReturnIfError;
  }
  return jsonParse(astrJson,aReturnIfError);
};
exports.jsonParseMaybe = jsonParseMaybe;

function jsonParseArray(astrJson,aDefault) {
  aDefault = aDefault || [];
  if (!astrJson || !astrJson["length"] || astrJson.length == 0) {
    return aDefault;
  }

  try {
    var ret = JSON.parse(astrJson);
    if (!NI.isArrayLike(ret)) {
      throw new Error("JSON parsed is not an array !");
    }
    return ret;
  }
  catch (e) {
    NI.error("NI.jsonParseArray: " + e);
    return aDefault;
  }
};
exports.jsonParseArray = jsonParseArray;

function jsonParseObject(astrJson,aDefault) {
  aDefault = aDefault || {};
  if (!astrJson || !astrJson["length"] || astrJson.length == 0) {
    return aDefault;
  }

  try {
    var ret = JSON.parse(astrJson);
    if (NI.objectType(ret) != "object") {
      throw new Error("JSON parsed is not an object !");
    }
    return ret;
  }
  catch (e) {
    NI.error("NI.jsonParseObject: " + e);
    return aDefault;
  }
};
exports.jsonParseObject = jsonParseObject;

function jsonToString (aJson, aReplacer, aSpaces) {
  if (aJson === undefined) {
    return "";
  }

  try {
    return JSON.stringify(aJson,aReplacer,aSpaces);
  }
  catch(e) {
    NI.error("JSON.stringify: " + e);
    return "";
  }
};
exports.jsonToString = jsonToString;

function jsonToIndentedString (aJson, aReplacer, aSpaces) {
  return NI.jsonToString(aJson,aReplacer,aSpaces||' ');
};
exports.jsonToIndentedString = jsonToIndentedString;

function lazyProperty(aObj, aVarName, aValueFunction) {
  aObj.__defineGetter__(aVarName, function() {
    var saved_value = aValueFunction();
    aObj.__defineGetter__(aVarName, function() {
      return saved_value;
    });
    return saved_value;
  });
}
exports.lazyProperty = lazyProperty;

//======================================================================
// ASSERT
//======================================================================
function invariant(condition, format) {
  if (!__DEV__) {
  if (format === undefined) {
    throw new Error('invariant requires an error message argument');
  }
  }
  if (!condition) {
    var error;
    if (format === undefined) {
      error = new Error(
        'Minified exception occurred; use the non-minified dev environment ' +
          'for the full error message and additional helpful warnings.'
      );
    }
    else {
      var args = Array.prototype.slice.call(arguments,2);
      error = new Error(
        "Invariant Violation: " +
          NI.vformat(format, args))
    }
    error.framesToPop = 1; // we don't care about invariant's own frame
    throw error;
  }
};
exports.invariant = invariant;

var assert = (function() {
  var K = {};

  var newAssertError = function(aMsg, aOptionalMsg) {
    if (isFunction(aOptionalMsg)) {
      aOptionalMsg = aOptionalMsg(aMsg);
    }
    return new Error(
      aMsg +
      (aOptionalMsg ? (": " + aOptionalMsg) : ""));
  }

  /**
   * Unreachable code
   * @param {string=} aOptionalMsg
   */
  K.unreachable = function (aOptionalMsg) {
    throw newAssertError("ASSERT.unreachable", aOptionalMsg);
  };

  /**
   * Check whether A is defined, i.e. strictly not equal to undefined.
   * @param {string=} aOptionalMsg
   */
  K.isDefined = function (A, aOptionalMsg) {
    if (A === undefined) {
      throw newAssertError("ASSERT.isDefined: " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether A is undefined, i.e. strictly equal to undefined.
   * @param {string=} aOptionalMsg
   */
  K.isUndefined = function (A, aOptionalMsg) {
    if (A !== undefined) {
      throw newAssertError("ASSERT.isUndefined: " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether A is truthy.
   * @param {string=} aOptionalMsg
   */
  K.isTrue = function (A, aOptionalMsg) {
    if (!A) {
      throw newAssertError("ASSERT.isTrue: " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether A is falsy.
   */
  K.isFalse = function (A, aOptionalMsg) {
    if (A) {
      throw newAssertError("ASSERT.isFalse: " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether A is a number.
   * @param {string=} aOptionalMsg
   */
  K.isNumber = function (A, aOptionalMsg) {
    if (!NI.isNumber(A)) {
      throw newAssertError("ASSERT.isNumber: " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether A is a string.
   * @param {string=} aOptionalMsg
   */
  K.isString = function (A, aOptionalMsg) {
    if (!NI.isString(A)) {
      throw newAssertError("ASSERT.isString: " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether A is a object.
   * @param {object=} aOptionalMsg
   */
  K.isObject = function (A, aOptionalMsg) {
    if (!NI.isObject(A)) {
      throw newAssertError("ASSERT.isObject: " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether A is an object literal.
   * @param {object=} aOptionalMsg
   */
  K.isObjectLiteral = function (A, aOptionalMsg) {
    if (!NI.isObjectLiteral(A)) {
      throw newAssertError("ASSERT.isObjectLiteral: " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether A is a function.
   * @param {function=} aOptionalMsg
   */
  K.isFunction = function (A, aOptionalMsg) {
    if (!NI.isFunction(A)) {
      throw newAssertError("ASSERT.isFunction: " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether A is a promise.
   * @param {promise=} aOptionalMsg
   */
  K.isPromise = function (A, aOptionalMsg) {
    if (!NI.isPromise(A)) {
      throw newAssertError("ASSERT.isPromise: " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether A is NaN.
   * @param {string=} aOptionalMsg
   */
  K.isNaN = function (A, aOptionalMsg) {
    if (!NI.isStrictlyNaN(A)) {
      throw newAssertError("ASSERT.isNaN: " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether A is not NaN.
   * @param {string=} aOptionalMsg
   */
  K.isNotNaN = function (A, aOptionalMsg) {
    if (NI.isStrictlyNaN(A)) {
      throw newAssertError("ASSERT.isNotNaN: " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether A is at least B, i.e. A >= B.
   * @param {string=} aOptionalMsg
   */
  K.atLeast = function (A,B, aOptionalMsg) {
    if (!(A >= B)) {
      throw newAssertError("ASSERT.atLeast: " + NI.toDebugString(A) +
                      " >= " + NI.toDebugString(B), aOptionalMsg);
    }
  };

  /**
   * Check whether A is at most B, i.e. A <= B.
   * @param {string=} aOptionalMsg
   */
  K.atMost = function (A,B, aOptionalMsg) {
    if (!(A <= B)) {
      throw newAssertError("ASSERT.atMost: " + NI.toDebugString(A) +
                      " <= " + NI.toDebugString(B), aOptionalMsg);
    }
  };

  /**
   * Check whether A is between B & C, i.e. A >= B && A <= C.
   * @param {string=} aOptionalMsg
   */
  K.between = function (A,B,C, aOptionalMsg) {
    if (!((A >= B) && (A <= C))) {
      throw newAssertError("ASSERT.atMost: " +
                      "(" + NI.toDebugString(A) + " >= " + NI.toDebugString(B) + ") && " +
                      "(" + NI.toDebugString(A) + " <= " + NI.toDebugString(C) + ") && ", aOptionalMsg);
    }
  };

  /**
   * Check whether A loosly equals B, i.e. A == B.
   * @param {string=} aOptionalMsg
   */
  K.equal = function (A,B, aOptionalMsg) {
    if (A != B) {
      throw newAssertError("ASSERT.equal: " + NI.toDebugString(A) +
                      " == " + NI.toDebugString(B), aOptionalMsg);
    }
  };
  K.equals = K.equal;

  /**
   * Check whether A strictly equals B, i.e. A === B.
   * @param {string=} aOptionalMsg
   */
  K.strictlyEqual = function (A,B, aOptionalMsg) {
    if (A !== B) {
      throw newAssertError("ASSERT.strictlyEqual: " + NI.toDebugString(A) +
                      " === " + NI.toDebugString(B), aOptionalMsg);
    }
  };

  /**
   * Check whether A is loosly different from B, i.e. A != B.
   * @param {string=} aOptionalMsg
   */
  K.notEqual = function (A,B, aOptionalMsg) {
    if (A == B) {
      throw newAssertError("ASSERT.notEqual: " + NI.toDebugString(A) +
                      " != " + NI.toDebugString(B), aOptionalMsg);
    }
  };

  /**
   * Check whether A not strictly equals B, i.e. A !== B.
   * @param {string=} aOptionalMsg
   */
  K.notStrictlyEqual = function (A,B, aOptionalMsg) {
    if (A === B) {
      throw newAssertError("ASSERT.notStrictlyEqual: " + NI.toDebugString(A) +
                      " !=== " + NI.toDebugString(B), aOptionalMsg);
    }
  };

  /**
   * Check whether the property or index B is in the object/array A.
   * @param {string=} aOptionalMsg
   */
  K.has = function (A,B, aOptionalMsg) {
    if ((typeof A != "object") || !(B in A)) {
      throw newAssertError("ASSERT.has: " + B + " not in " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether the property or index B is in the object/array A and its value is defined.
   * @param {string=} aOptionalMsg
   */
  K.hasDefined = function (A,B, aOptionalMsg) {
    if ((typeof A != "object") || !(B in A) || (A[B] === undefined)) {
      throw newAssertError("ASSERT.hasDefined: " + B + " not in " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether the property or index B is missing from the object/array A.
   * @param {string=} aOptionalMsg
   */
  K.hasnt = function (A,B, aOptionalMsg) {
    if ((typeof A == "object") && (B in A)) {
      throw newAssertError("ASSERT.hasnt: " + B + " is in " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether the property or index B is in missing from the object/array A
   * or its value is undefined.
   * @param {string=} aOptionalMsg
   */
  K.hasntDefined = function (A,B, aOptionalMsg) {
    if ((typeof A == "object") && (B in A) && (A[B] !== undefined)) {
      throw newAssertError("ASSERT.hasntDefined: " + B + " is in " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether A is an instance of B.
   * @param {string=} aOptionalMsg
   */
  K.isInstanceOf = function (A,B, aOptionalMsg) {
    if (!(A instanceof B)) {
      var typeB = NI.toDebugString(B);
      if (NI.stringContains(typeB,"function ")) {
        typeB = NI.stringAfter(typeB,"function ");
        typeB = NI.stringBefore(typeB,"(");
      }
      throw newAssertError("ASSERT.isInstanceOf: " +
                      "'" + NI.toDebugString(A) + "'" +
                      " not an instance of " +
                      "'" + typeB + "'", aOptionalMsg);
    }
  };

  /**
   * Check whether the typeof A equals B.
   * @param {string=} aOptionalMsg
   */
  K.isTypeOf = function (A,B, aOptionalMsg) {
    if (typeof(A) != B) {
      throw newAssertError("ASSERT.isTypeOf: " +
                      NI.toDebugString(A) + " not a typeof " + B, aOptionalMsg);
    }
  };

  /**
   * Check whether A is "array like".
   * @param {string=} aOptionalMsg
   */
  K.isArrayLike = function (A, aOptionalMsg) {
    if (!NI.isArrayLike(A)) {
      throw newAssertError("ASSERT.isArrayLike: " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether A is an array.
   * @param {string=} aOptionalMsg
   */
  K.isArray = function (A, aOptionalMsg) {
    if (!NI.isArray(A)) {
      throw newAssertError("ASSERT.isArray: " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether A is a function.
   * @param {string=} aOptionalMsg
   */
  K.isFunction = function (A, aOptionalMsg) {
    if (!NI.isFunction(A)) {
      throw newAssertError("ASSERT.Function: " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  /**
   * Check whether the specified function raises the specified exception.
   * @param {string=} aOptionalMsg
   */
  K.raises = function(A,aThrow, aOptionalMsg) {
    var didThrow;
    try {
      A();
    }
    catch (e) {
      didThrow = e;
      if (didThrow == aThrow ||
          ((aThrow instanceof Error) && (didThrow instanceof Error) &&
           aThrow.message == didThrow.message)
         ) {
        return;
      }
      else {
        throw newAssertError("ASSERT.raises: raised unexpected exception: "+NI.toDebugString(didThrow), aOptionalMsg);
      }
    }

    throw newAssertError("ASSERT.raises: did not raise an expection", aOptionalMsg);
  };

  /**
   * Check whether A is a string and that it's not empty.
   * @param {string=} aOptionalMsg
   */
  K.stringNotEmpty = function (A, aOptionalMsg) {
    if (!A || (typeof A != "string") || (A.length < 1)) {
      throw newAssertError("ASSERT.stringNotEmpty: " + NI.toDebugString(A), aOptionalMsg);
    }
  };

  return K;
}());
exports.assert = assert;

//======================================================================
// Timing stuff (Node)
//======================================================================
if (__BACKEND__ && !NI.global.isReactNative) {
  exports.callNextTick = function callNextTick(aHandler, aTimeout) {
    if (!aTimeout) {
      process.nextTick(aHandler);
    }
    else {
      setTimeout(aHandler, aTimeout || 1);
    }
  };

  exports.timerInSeconds = function timerInSeconds() {
    var hrtime = process.hrtime();
    return hrtime[0] + (hrtime[1]/1e9);
  }
}
else {
  exports.callNextTick = function callNextTick(aHandler, aTimeout) {
    setTimeout(aHandler, aTimeout || 1);
  };
}

// Cancelable delay (timeout), returns a Promise.
function delay(ms) {
  var ctr, rej;
  var p = NI.Promise(function (resolve, reject) {
    ctr = setTimeout(resolve, ms);
    rej = reject;
  });
  p.cancel = function() {
    clearTimeout(ctr); rej(ErrorFmt("Cancelled"));
    return p;
  }
  return p;
}
exports.delay = delay;

//======================================================================
// UrlSafe Base64
//======================================================================
function b64encodeBuffer(buffer) {
  if (!Buffer.isBuffer(buffer)) {
    buffer = new Buffer(buffer);
  }
  return buffer.toString('base64')
    .replace(/\+/g, '-') // Convert '+' to '-'
    .replace(/\//g, '_') // Convert '/' to '_'
    .replace(/=+$/, ''); // Remove ending '='
};
exports.b64encodeBuffer = b64encodeBuffer;

function b64decodeBuffer(base64) {
  base64 = base64
    .replace(/\-/g, '+')  // Convert '-' to '+'
    .replace(/\_/g, '/'); // Convert '_' to '/'
  switch (base64.length % 4) { // Pad with trailing '='s
  case 0:
    break; // No pad chars in this case
  case 2:
    base64 += "==";
    break; // Two pad chars
  case 3:
    base64 += "=";
    break; // One pad char
  default:
    throw new Error("Illegal base64url string!");
  }
  return new Buffer(base64, 'base64');
};
exports.b64decodeBuffer = b64decodeBuffer;

function b64encodeHex(aHex) {
  var b = new Buffer(aHex,'hex');
  return NI.b64encodeBuffer(b);
};
exports.b64encodeHex = b64encodeHex;

function b64decodeHex(aBase64) {
  return NI.b64decodeBuffer(aBase64).toString('hex');
};
exports.b64decodeHex = b64decodeHex;

//======================================================================
// sprintf
//======================================================================
(function(K) {
  var re = {
    not_string: /[^sjJkK]/,
    number: /[dief]/,
    text: /^[^\x25]+/,
    modulo: /^\x25{2}/,
    placeholder: /^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fiosuxXjJkK])/,
    key: /^([a-z_][a-z_\d]*)/i,
    key_access: /^\.([a-z_][a-z_\d]*)/i,
    index_access: /^\[(\d+)\]/,
    sign: /^[\+\-]/
  }

  function sprintf() {
    if (arguments.length <= 1) {
      return arguments[0];
    }
    var key = arguments[0], cache = sprintf.cache
    if (!(cache[key] && hasProperty(cache,key))) {
      cache[key] = sprintf.parse(key)
    }
    return sprintf.format.call(null, cache[key], arguments)
  }

  sprintf.format = function(parse_tree, argv) {
    var cursor = 1, tree_length = parse_tree.length, node_type = "", arg, output = [], i, k, match, pad, pad_character, pad_length, is_positive = true, sign = ""
    for (i = 0; i < tree_length; i++) {
      node_type = get_type(parse_tree[i])
      if (node_type === "string") {
        output[output.length] = parse_tree[i]
      }
      else if (node_type === "array") {
        match = parse_tree[i] // convenience purposes only
        if (match[2]) { // keyword argument
          arg = argv[cursor]
          for (k = 0; k < match[2].length; k++) {
            if (!hasProperty(arg,match[2][k])) {
              throw new Error(sprintf("[sprintf] property '%s' does not exist", match[2][k]))
            }
            arg = arg[match[2][k]]
          }
        }
        else if (match[1]) { // positional argument (explicit)
          arg = argv[match[1]]
        }
        else { // positional argument (implicit)
          arg = argv[cursor++]
        }

        if (re.not_string.test(match[8]) && (get_type(arg) != "number" && isNaN(arg))) {
          match[8] = "s";
          // throw new TypeError(sprintf("[sprintf] expecting number but found %s", get_type(arg)))
        }

        if (re.number.test(match[8])) {
          is_positive = arg >= 0
        }

        switch (match[8]) {
        case "b":
          arg = arg.toString(2)
          break
        case "c":
          arg = String.fromCharCode(arg)
          break
        case "d":
        case "i":
          arg = parseInt(arg, 10)
          break
        case "e":
          arg = match[7] ? arg.toExponential(match[7]) : arg.toExponential()
          break
        case "f":
          arg = match[7] ? parseFloat(arg).toFixed(match[7]) : parseFloat(arg)
          break
        case "o":
          arg = arg.toString(8)
          break
        case "s":
          arg = ((arg = String(arg)) && match[7] ? arg.substring(0, match[7]) : arg)
          break
        case "u":
          arg = arg >>> 0
          break
        case "x":
          arg = arg.toString(16)
          break
        case "X":
          arg = arg.toString(16).toUpperCase()
          break
        case "j":
          if (isObject(arg)) {
            arg = jsonDecycle(arg);
          }
          arg = NI.jsonToString(arg);
          break
        case "J":
          if (isObject(arg)) {
            arg = jsonDecycle(arg);
          }
          arg = NI.jsonToString(arg,undefined,' ');
          break
        case "k":
          arg = NI.toDebugString(arg);
          break
        case "K":
          arg = NI.toDebugString(arg,' ');
          break
        }
        if (re.number.test(match[8]) && (!is_positive || match[3])) {
          sign = is_positive ? "+" : "-"
          arg = arg.toString().replace(re.sign, "")
        }
        else {
          sign = ""
        }
        pad_character = match[4] ? match[4] === "0" ? "0" : match[4].charAt(1) : " "
        pad_length = match[6] - (sign + arg).length
        pad = match[6] ? (pad_length > 0 ? str_repeat(pad_character, pad_length) : "") : ""
        output[output.length] = match[5] ? sign + arg + pad : (pad_character === "0" ? sign + pad + arg : pad + sign + arg)
      }
    }
    return output.join("")
  }

  sprintf.cache = {}

  sprintf.parse = function(fmt) {
    var _fmt = fmt, match = [], parse_tree = [], arg_names = 0
    while (_fmt) {
      if ((match = re.text.exec(_fmt)) !== null) {
        parse_tree[parse_tree.length] = match[0]
      }
      else if ((match = re.modulo.exec(_fmt)) !== null) {
        parse_tree[parse_tree.length] = "%"
      }
      else if ((match = re.placeholder.exec(_fmt)) !== null) {
        if (match[2]) {
          arg_names |= 1
          var field_list = [], replacement_field = match[2], field_match = []
          if ((field_match = re.key.exec(replacement_field)) !== null) {
            field_list[field_list.length] = field_match[1]
            while ((replacement_field = replacement_field.substring(field_match[0].length)) !== "") {
              if ((field_match = re.key_access.exec(replacement_field)) !== null) {
                field_list[field_list.length] = field_match[1]
              }
              else if ((field_match = re.index_access.exec(replacement_field)) !== null) {
                field_list[field_list.length] = field_match[1]
              }
              else {
                throw new SyntaxError("[sprintf] failed to parse named argument key")
              }
            }
          }
          else {
            throw new SyntaxError("[sprintf] failed to parse named argument key")
          }
          match[2] = field_list
        }
        else {
          arg_names |= 2
        }
        if (arg_names === 3) {
          throw new Error("[sprintf] mixing positional and named placeholders is not (yet) supported")
        }
        parse_tree[parse_tree.length] = match
      }
      else {
        throw new SyntaxError("[sprintf] unexpected placeholder: " + _fmt)
      }
      _fmt = _fmt.substring(match[0].length)
    }
    return parse_tree
  }

  var vsprintf = function(fmt, argv, _argv) {
    _argv = (argv || []).slice(0)
    _argv.splice(0, 0, fmt)
    return sprintf.apply(null, _argv)
  }

  /**
   * helpers
   */
  function get_type(variable) {
    return Object.prototype.toString.call(variable).slice(8, -1).toLowerCase()
  }

  function str_repeat(input, multiplier) {
    return Array(multiplier + 1).join(input)
  }

  K.sprintf = sprintf;
  K.vsprintf = vsprintf;
})(exports);

exports.format = exports.sprintf;
exports.vformat = exports.vsprintf;

function _formatSingleError(err,index) {
  var msg = "";
  if (err.message) {
    msg += NI.stringTrim("" + err.message);
  }
  else if (err.error) {
    msg += NI.stringTrim("" + err.error);
  }
  else if (NI.isObject(err)) {
    msg += NI.format("%j", err);
  }
  else {
    msg += NI.stringTrim(""+err);
  }
  if (err.desc) {
    msg += ": " + err.desc;
  }

  // PGSQL error?
  if (NI.stringContains(msg,"PGSQL Query Error:")) {
    msg = NI.stringBefore(msg,"\n");
    if (NI.stringContains(msg,": error:")) {
      msg = NI.stringAfter(msg,": error:");
    }
    else {
      msg = NI.stringAfter(msg,"PGSQL Query Error:");
    }
  }
  // MySQL error?
  else if (NI.stringContains(msg,"ER_SIGNAL_EXCEPTION:")) {
    msg = NI.stringBefore(msg,"\n");
    msg = NI.stringAfter(msg,"ER_SIGNAL_EXCEPTION:");
  }
  // Other log based error?
  else if (NI.stringContains(msg,". [")) {
    msg = NI.stringBefore(msg,". [") + ".";
  }
  else if (NI.stringContains(msg," [") && NI.stringEndsWith(msg,"]")) {
    msg = NI.stringBefore(msg," [");
  }

  if (index !== undefined) {
    return NI.format("ERROR%d: %s", index+1, NI.stringTrim(msg));
  }
  else {
    return NI.stringTrim(msg);
  }
}
function formatError(aErrors,aErrorList) {
  if (NI.isArray(aErrors)) {
    var ret = "";
    NI.forEach(aErrors || {}, function(err,index) {
      var r = _formatSingleError(err,index);
      if (aErrorList) {
        aErrorList.push(r);
      }
      else {
        ret += r + "\n";
      }
    });
    return aErrorList ? aErrorList : ret;
  }
  else {
    var r = _formatSingleError(aErrors);
    if (aErrorList) {
      return aErrorList.push(r);
    }
    else {
      return r;
    }
  }
}
exports.formatError = formatError;

//======================================================================
// Url utils
//======================================================================

function getQueryParamByName(name, url) {
  url = url || NI.selectn('location.search', NI.global) || "";
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
  results = regex.exec(url);
  return (!results) ? undefined : decodeURIComponent(results[1].replace(/\+/g, " "));
}
NI.getQueryParamByName = getQueryParamByName;

function updateQueryParamByName(key, value, url) {
  url = url || NI.selectn('location.href', NI.global);
  value = NI.stringEncodeURI(value);
  var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi");
  var hash;
  if (re.test(url)) {
    if (typeof value !== 'undefined' && value !== null) {
      return url.replace(re, '$1' + key + "=" + value + '$2$3');
    }
    else {
      hash = url.split('#');
      url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
      if (typeof hash[1] !== 'undefined' && hash[1] !== null) {
        url += '#' + hash[1];
      }
      return url;
    }
  }
  else {
    if (typeof value !== 'undefined' && value !== null) {
      var separator = url.indexOf('?') !== -1 ? '&' : '?';
      hash = url.split('#');
      url = hash[0] + separator + key + '=' + value;
      if (typeof hash[1] !== 'undefined' && hash[1] !== null) {
        url += '#' + hash[1];
      }
      return url;
    }
    else {
      return url;
    }
  }
}
NI.updateQueryParamByName = updateQueryParamByName;

function queryParamsToObject(aQueryParams) {
  aQueryParams = aQueryParams || NI.selectn('location.search', NI.global) || "";
  var queryDict = {}
  if (NI.isEmpty(aQueryParams)) {
    return {};
  }
  if (NI.stringContains(aQueryParams,"?")) {
    aQueryParams = NI.stringAfter(aQueryParams,"?");
  }
  NI.forEach(aQueryParams.split("&"), function(item) {
    var split = item.split("=");
    queryDict[decodeURIComponent(split[0])] = decodeURIComponent(split[1])
  })
  return queryDict;
}
NI.queryParamsToObject = queryParamsToObject;

if (!__BACKEND__) {
  function updateWindowUrl(aNewUrl, aPushState) {
    if (!aNewUrl) {
      return;
    }
    if (aPushState === "refresh") {
      window.location.href = aNewUrl;
    }
    else if (NI.isFunction(aPushState)) {
      if (window.history.pushState) {
        window.history.pushState(null, null, aNewUrl);
        window.onpopstate = aPushState
      }
    }
    else if (aPushState) {
      throw NI.Error("Invalid aPushState should be 'refresh' or a function, is '%s'.",
                     NI.objectType(aPushState));
    }
    else {
      if (window.history.replaceState) {
        window.history.replaceState(null, null, aNewUrl);
      }
    }
  }
  NI.updateWindowUrl = updateWindowUrl;
}

function objectToQueryString(obj, prefix) {
  var str = [];
  for(var p in obj) {
    if (hasProperty(obj,p)) {
      var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
      str.push(typeof v == "object" ?
               objectToQueryString(v, k) :
               encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
  }
  return str.join("&");
}
NI.objectToQueryString = objectToQueryString;

function parseHashBang(aURL) {
  aURL = aURL || NI.selectn('location.href', NI.global);
  var vars = {};
  var hashes = aURL.slice(aURL.indexOf('#') + 1).split('?');
  if(hashes.length > 1) {
    vars['page'] = hashes[0];
    hashes = hashes[1].split('&');
  } else {
    hashes = hashes[0].split('&');
  }

  for(var i = 0; i < hashes.length; i++) {
    var hash = hashes[i].split('=');
    if(hash.length > 1) {
      vars[hash[0]] = hash[1];
    } else {
      vars[hash[0]] = null;
    }
  }

  return vars;
}
NI.parseHashBang = parseHashBang;

function buildUrl(aBase,aParams) {
  var url = aBase;
  if (aParams) {
    url += "?";
    url += objectToQueryString(aParams);
  }
  return url;
}
NI.buildUrl = buildUrl;

//======================================================================
// Fetch
//======================================================================
NI.fetch = function() {
  if (typeof fetch === "undefined") {
    // npm install --save isomorphic-fetch
    NI.fetch = require('isomorphic-fetch');
  }
  else {
    var builtinFetch = fetch;
    /* Nothing to do, fetch is already defined */
    NI.fetch = function fetch(url,params) { return builtinFetch(url,params) };
  }
  return NI.fetch.apply(this,arguments);
}

function encodeFormData(encodedBody,k,v) {
  if (encodedBody) {
    encodedBody += "&";
  }
  else {
    encodedBody = "";
  }
  encodedBody += NI.stringEncodeURI(k);
  encodedBody += "=";
  encodedBody += NI.stringEncodeURI(v);
  return encodedBody;
}
exports.encodeFormData = encodeFormData;

if (__BACKEND__) {
  function streamToBuffer(aStream) {
    return NI.Promise(function(aResolve,aReject) {
      var stream = aStream;
      var bufs = [];
      stream.on('data', function(d) {
        bufs.push(d);
      });
      stream.on('end', function() {
        aResolve(Buffer.concat(bufs));
      });
      stream.on('error', function(err) {
        aReject(err);
      });
    })
  }
  exports.streamToBuffer = streamToBuffer;
}

//======================================================================
// Promise
//======================================================================
if (typeof Promise === "undefined") {
  // npm install --save es6-promise
  NI.global.Promise = require('es6-promise').Promise;
}

// Note: A promise's body is executed 'as soon as the promise is created',
// meaning that it'll be queued immediatly for execution even if there's no
// .then() or .catch() call. It is important to remember this if you're using
// a Promise object lazily since creating the promise will execute its body
// 'immediatly'; in the case where you do not want the promise to run you
// should wrap its creation in a local function that is called only when the
// promise's body can be called.
NI.Promise = function(aCallback) {
  if (NI.isPromise(aCallback)) {
    return aCallback;
  }
  else if (NI.isFunction(aCallback)) {
    return new Promise(aCallback);
  }
  else {
    return Promise.resolve(aCallback);
  }
}

function series(aFuncs,aParam0) {
  var numFuncs = aFuncs.length;
  if (numFuncs <= 0) {
    return undefined;
  }

  NI.assert.isFunction(aFuncs[0]);
  var p = NI.Promise(function(aResolve, aReject) {
    try {
      aResolve(aFuncs[0](aParam0));
    }
    catch (e) {
      aReject(e)
    }
  })
  for (var i = 1; i < numFuncs; ++i) {
    NI.assert.isFunction(aFuncs[i]);
    (function(f) {
      p = p.then(function(r) { return f(r); })
    })(aFuncs[i])
  }
  return p;
}
NI.series = series;

function promiseWaitAll(aPromises) {
  return Promise.all(aPromises);
}
NI.waitAll = promiseWaitAll;

function promiseRace(aPromises) {
  return Promise.race(aPromises);
}
NI.race = promiseRace;

function promiseResolve(aValueOrPromise) {
  return Promise.resolve(aValueOrPromise);
}
NI.resolve = promiseResolve;

function promiseReject(aValueOrPromise) {
  return Promise.reject(aValueOrPromise);
}
NI.reject = promiseReject;

if (__BACKEND__) {
  function denodeify(aNodeStyleFunction, aFilter, aThat) {
		'use strict';

		return function() {
			var that = aThat || this; // eslint-disable-line
			var functionArguments = new Array(arguments.length + 1);

			for (var iFun = 0; iFun < arguments.length; iFun += 1) {
				functionArguments[iFun] = arguments[iFun];
			}

			function promiseHandler(resolve, reject) {
				function callbackFunction() {
					var args = new Array(arguments.length);

					for (var iArg = 0; iArg < args.length; iArg += 1) {
						args[iArg] = arguments[iArg];
					}

					if (aFilter) {
						args = aFilter.apply(that, args);
					}

					var error = args[0];
					var result = args[1];

					if (error) {
						return reject(error);
					}

					return resolve(result);
				}

				functionArguments[functionArguments.length - 1] = callbackFunction;
				aNodeStyleFunction.apply(that, functionArguments);
			}

			return new Promise(promiseHandler);
		};
  }
  NI.denodeify = denodeify;
  NI.promisify = denodeify;

  var fs = require('fs');
  function readFile(aPath, aEncoding) {
    return NI.Promise(function(aResolve, aReject) {
      fs.readFile(aPath, aEncoding, function(aErr, aData) {
        if (aErr) {
          aReject(aErr);
        }
        else {
          aResolve(aData);
        }
      });
    });
  }
  NI.readFile = readFile;
}

//======================================================================
// HTTP request. JSON by default, use an empty textData to force raw text.
//======================================================================
NI.httpRequest = function(aRequest) {
  var url;
  var params = {
    credentials: 'same-origin', // send cookies
    headers: {}
  }
  var isJson = !aRequest.textData;
  var responseJson = ("jsonResponse" in aRequest) ? aRequest.jsonResponse : isJson;

  // initialize the header
  if (isJson) {
    params.headers['Accept'] = 'application/json';
    params.headers['Content-Type'] = 'application/json';
  }
  if (!NI.isEmpty(aRequest.headers)) {
    params.headers = NI.assignClone(aRequest.headers, params.headers);
  }

  // set the request method
  if (aRequest.url !== undefined) {
    url = aRequest.url;
    params.method = aRequest.method;
  }
  else if (aRequest.post !== undefined) {
    url = aRequest.post;
    params.method = "post";
  }
  else if (aRequest.get !== undefined) {
    url = aRequest.get;
    params.method = "get";
  }

  // set the request data
  if (params.method === "get") {
    if (aRequest.jsonData) {
      url += "?" + NI.objectToQueryString(aRequest.jsonData);
    }
    else if (aRequest.textData) {
      url += "?" + aRequest.textData;
    }
  }
  else if (params.method === "post") {
    if (aRequest.jsonData) {
      params.body = NI.jsonToString(aRequest.jsonData);
    }
    else if (aRequest.textData) {
      params.body = aRequest.textData;
    }
  }
  else {
    throw NI.Error("Neither get nor post has been specified in the handler, got '%s'.", params.method);
  }

  // NI.log("... NI.httpRequest: %s: %K",url, params);

  // do the fetching
  var fetchPromise = NI.fetch(url, params).then(function(response) {
    if (response.status == 200) {
      if (responseJson) {
        return response.text().then(function(text) {
          return NI.jsonParseObject(text,text);
        });
      }
      else {
        return response.text();
      }
    }
    else if (response.status == 400) {
      if (responseJson) {
        return response.text().then(function(text) {
          var data = NI.jsonParseObject(text,text);
          data.responseStatus = response.status;
          data.responseStatusText = response.statusText;
          throw data;
        });
      }
      else {
        return response.text().then(function(text) {
          throw NI.Error("Server error (%d %s %s): %s: %s",
                         response.status,
                         params.method.toUpperCase(),url,
                         response.statusText,
                         text);
        });
      }
    }
    else {
      throw NI.Error("Server error at (%d %s %s): %s",
                     response.status,
                     params.method.toUpperCase(),url,
                     response.statusText);
    }
  });

  if (!aRequest.onReply) {
    // no onReply, return a promise
    return fetchPromise;
  }
  else {
    // onReply callback
    return fetchPromise.then(function(data) {
      aRequest.onReply(data);
    })['catch'](function(aFetchError) {
      if (aRequest.onError) {
        aRequest.onError(aFetchError);
      }
    });
  }
}

//======================================================================
// GQL request.
//======================================================================
NI.GQLRequest = function(aGQLParams, aGQLEndPointUrl) {
  aGQLEndPointUrl = aGQLEndPointUrl || callOrGet(window.GQLRequestDefaultUrl) || (window.location.origin + '/api/gql');
  var r = NI.Promise(function(aResolve, aReject) {
    NI.httpRequest({
      post: aGQLEndPointUrl,
      jsonData: aGQLParams,
      onReply: function(aResult) {
        if (aResult && aResult.errors && !aGQLParams.resolveErrors) {
          NI.error("GQL Request errors: %K", aResult.errors)
          aReject(aResult.errors)
        }
        else {
          aResolve(aResult);
        }
      },
      onError: aReject
    });
  });
  if (aGQLParams.filter) {
    return r.then(function(aResult) {
      return aGQLParams.filter(aResult);
    });
  }
  else {
    return r;
  }
};

//======================================================================
// GQL formatting.
//======================================================================
var GQLQueryOps = {
  query: 'query',
  mutation: 'mutation',
}

function getGQLQueryOp(aOp) {
  if(GQLQueryOps[aOp]) {
    return GQLQueryOps[aOp]
  } else {
    throw new Error(NI.format("Unexpected GQL query op %j !!", aOp))
  }
}

function getGQLQuerySel2(aFields) {
  return NI.map(aFields, function(value, field) {
    // aFields == {aField1: ...}
    if(NI.isString(field)) {

      // aFields == {aField1: [...]}
      if(NI.isArray(value)) {

        // aFields == {aField1: [{aFieldArg1: aFieldVal1, ...}, [aField1a, aField1b, ...]}
        if(value.length == 2 && NI.isArray(value[1]) && NI.isObjectLiteral(value[0])) {
          return getGQLQuerySel(field, value[0], value[1])

        // aFields == {aField1: [aField1a, aField1b, ...]}
        } else {
          return getGQLQuerySel(field, {}, value)
        }

      // Unexpected !!
      } else {
        throw new Error(NI.format(
          "aFields must be any of: %s OR %s !!",
          "{aField1: [{aFieldArg1: aFieldVal1, ...}, [aField1a, aField1b, ...]}",
          "{aField1: [aField1a, aField1b, ...]}"
        ))
      }

    // aFields == [aField1, aField2, ...]
    } else {

      // aFields == [..., aString1, ...]
      if(NI.isString(value)) {
        return value

      // aFields == [..., aObject1, ...]
      } else if(NI.isObjectLiteral(value)) {
        return getGQLQuerySel2(value)

      // Unexpected !!
      } else {
        throw new Error("aFields must only contain strings or object-literals !!");
      }
    }
  }).join(" ")
}

function getGQLQuerySel(aFuncName, aArgs, aFields) {
  NI.assert.isObjectLiteral(aArgs);
  NI.assert.isString(aFuncName); // TODO: check for supported GQL funcs

  if(NI.objectKeys(aArgs).length) {
    return NI.format(
      "%s(%s) { %s }",
      aFuncName,
      NI.format("%j", aArgs).replace(/\"([^"]+)\":/g,"$1:").slice(1,-1),
      getGQLQuerySel2(aFields)
    )
  } else {
    return NI.format(
      "%s { %s }",
      aFuncName,
      getGQLQuerySel2(aFields)
    )
  }
}

exports.formatGQL = function(aOp, aFuncNameOrQueries, aArgsOrFields, aFieldsOrNull) {
  if(NI.isArray(aFuncNameOrQueries)) {
    var queries = NI.map(aFuncNameOrQueries, function(query, nth) {
      return "q" + nth + ": " + getGQLQuerySel.apply(undefined, query);
    });

    return NI.format("%s _ { %s }", aOp, queries.join(", "));
  } else {
    var aFuncName = aFuncNameOrQueries;
    var aArgs = NI.isArray(aArgsOrFields) ? {} : aArgsOrFields;
    var aFields = NI.isArray(aArgsOrFields) ? aArgsOrFields : (aFieldsOrNull || []);

    if(aFields.length) {
      return NI.format(
        "%s _ { %s }",
        getGQLQueryOp(aOp),
        getGQLQuerySel(aFuncName, aArgs, aFields)
      )
    } else {
      throw new Error("aFields must not be empty !!")
    }
  }
}
