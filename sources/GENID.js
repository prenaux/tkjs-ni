var NI = require('./NI');
var UUID = require('node-uuid');

// Generates a short unique-ish 10 characters id
module.exports.makeShortID = function() {
  return NI.b64encodeBuffer(NI.randomBytes(7));
};

// Generates a 32 characters UUID (hex encoded)
module.exports.makeUUIDHex = function() {
  var arr = new Array(16);
  UUID.v4(null, arr, 0);
  return NI.stringByteArray2HexString(arr);
};

// Generates a 22 characters UUID (base64 encoded)
module.exports.makeUUID = function() {
  var buf = new Buffer(16);
  UUID.v4(null, buf, 0);
  return NI.b64encodeBuffer(buf);
};

// Generates a short 6 digit token id, this id is *NOT* unique, it is always safe to store as a INT32
module.exports.makeTokenID = function() {
  return Math.floor(Math.random()*999999.0);
};
