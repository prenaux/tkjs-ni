tkjs-ni
=======

tkjs-ni is a javascript library that provides utility functions that can be
used in the browser, node.js and react-native.

tkjs-ni is MIT licensed, see the LICENSE section.

AUTHORS
=======

The following authors have all licensed their contributions to tkjs-ni
under the licensing terms detailed in the LICENSE section.

* Pierre Renaux <pierre@talansoft.com>

CONTRIBUTING
============

We actively welcome your pull requests.

To contribute submit a pull request and add your name & email to the AUTHORS
section of this file. We will not accept contributions if your name & email
isn't added to the AUTHORS section as it serves as proof of acknowledgement
that you have read and accepted the contribution terms described below.

1. The term "contribution" means any source code, object code, patch, tool,
   sample, graphic, specification, manual, documentation, or any other
   material posted or submitted by you to the project.
2. With respect to any worldwide copyrights, or copyright applications and
   registrations, in your contribution:
   - you assign to us joint ownership through your contribution, and to the
     extent that such assignment is or becomes invalid, ineffective or
     unenforceable, through your contribution you grant to us a perpetual,
     irrevocable, non-exclusive, worldwide, no-charge, royalty-free,
     unrestricted license to exercise all rights under those copyrights. This
     includes, at our option, the right to sublicense these same rights to
     third parties through multiple levels of sublicensees or other licensing
     arrangements;
   - you agree that each of us can do all things in relation to your
     contribution as if each of us were the sole owners, and if one of us
     makes a derivative work of your contribution, the one who makes the
     derivative work (or has it made) will be the sole owner of that
     derivative work;
   - you agree that you will not assert any moral rights in your contribution
     against us, our licensees or transferees;
   - you agree that we may register a copyright in your contribution and
     exercise all ownership rights associated with it; and
   - you agree that neither of us has any duty to consult with, obtain the
     consent of, pay, or give an accounting to the other for any use or
     distribution of your contribution.
3. With respect to any patents you own, or that you can license without
   payment to any third party, through your contribution you grant to us a
   perpetual, irrevocable, non-exclusive, worldwide, no-charge, royalty-free
   license to: make, have made, use, sell, offer to sell, import, and
   otherwise transfer your contribution in whole or in part, alone or in
   combination with or included in any product, work or materials arising out
   of the project to which your contribution was submitted, and at our option,
   to sublicense these same rights to third parties through multiple levels of
   sublicensees or other licensing arrangements.
4. Except as set out above, you keep all right, title, and interest in your
   contribution. The rights that you grant to us under these terms are
   effective on the date you first submitted a contribution to us.
5. With respect to your contribution, you represent that it is an original
   work and that you can legally grant the rights set out in these terms; it
   does not to the best of your knowledge violate any third party's
   copyrights, trademarks, patents, or other intellectual property rights; and
   you are authorized to accept this agreement on behalf of your company (if
   your contribution is on behalf of a company).

LICENSE
=======

```
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2018, Pierre Renaux, pierre@talansoft.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/**
 * sprintf (https://github.com/alexei/sprintf.js):
 * Copyright (c) 2007-present, Alexandru Mărășteanu <hello@alexei.ro>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 * * Neither the name of this software nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
```
